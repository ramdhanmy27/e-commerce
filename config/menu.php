<?php

return [
	"default" => env("MENU_DRIVER", "router"),

	"drivers" => [
		"db" => env("DB_CONNECTION", "pgsql"),
	]
];