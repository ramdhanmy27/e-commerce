<?php

return [
	"image" => [
		"url" => env("CATALOG_IMG_URL"),
		"default" => "img/assets/product.jpg",
	],
	"limitation" => [9, 36, 72],
];