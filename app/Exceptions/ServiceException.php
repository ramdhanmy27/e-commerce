<?php

namespace App\Exceptions;

use App\Services\ServiceResponse;

class ServiceException extends \Exception {

	private $messages = [];
	private $statusCode;

	public function __construct($messages, $code = 200, Exception $previous = null) {
		$this->messages = is_array($messages) ? $messages : [$messages];
		$this->statusCode = $code;

		parent::__construct(current($this->messages), $code, $previous);
	}

	public function getMessages() {
		return $this->messages;
	}

	public function response($format = "json") {
		return ServiceResponse::make([
			"status" => "error", 
			"statusCode" => $this->statusCode,
		], current($this->getMessages()), $format);
	}
}