<?php

namespace App\Models;

class StockAllocation extends Model
{
    protected $connection	=	"pgsql-backend";
    protected $table 		= 	"stock_allocation";
    protected $primaryKey	= 	"id_stock_allocation";
    public $timestamps 		= 	false;
}
