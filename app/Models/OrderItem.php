<?php

namespace App\Models;

class OrderItem extends Model
{
    protected $connection	=	"pgsql-backend";
    protected $table 		= 	"order_item";
    protected $primaryKey	= 	"id_order_item";
    public $timestamps 		= 	false;
}
