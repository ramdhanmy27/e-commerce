<?php

namespace App\Models;

class ProductSize extends Model
{
    protected $connection	=	"pgsql-backend";
    protected $table 		= 	"product_size";
    protected $primaryKey	= 	"code_size";
    public $incrementing	= 	false;
}
