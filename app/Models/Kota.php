<?php

namespace App\Models;

class Kota extends Model
{
    protected $connection = "pgsql-backend";
    protected $table = "kota";
    protected $primaryKey = "kopid";
    public $incrementing = false;

   	static function getKota($pid){
   		return self::where("pid",$pid)
   					->orderBy("kota_cabang")
   					->get()
   					->toArray();
   	}
}
