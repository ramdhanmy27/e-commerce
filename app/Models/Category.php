<?php

namespace App\Models;

class Category extends Model
{
    protected $connection = "pgsql-backend";
    protected $table = "category";
    protected $primaryKey = "code_category";
    public $incrementing = false;

    public function scopeLink($query, $link) {
    	return $query->where("category.name", "ilike", str_replace("-", " ", $link));
    }
}
