<?php

namespace App\Models;

class BankAccount extends Model
{
    protected $connection = "pgsql-backend";
    protected $table = "bank_account";
    protected $primaryKey = "code_bank";
    public $incrementing = false;
}
