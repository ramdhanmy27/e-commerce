<?php

namespace App\Models;

class Buyer extends Model
{
    protected $connection	=	"pgsql-backend";
    protected $table 		= 	"buyer";
    protected $primaryKey	= 	"code_buyer";
    public $incrementing	= 	false;
    public $timestamps 		= 	false;
}
