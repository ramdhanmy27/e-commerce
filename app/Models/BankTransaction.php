<?php

namespace App\Models;

class BankTransaction extends Model
{
    protected $connection = "pgsql-backend";
    protected $table = "bank_transaction";
    protected $primaryKey = "number_transaction";
    public $incrementing = false;
    public $timestamps = false;
}
