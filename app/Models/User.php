<?php 

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use DB;
use Hash;

class User extends Authenticatable
{
    use \Zizaco\Entrust\Traits\EntrustUserTrait;

    protected $fillable = ['name', 'email', 'password', "phone"];
    protected $hidden = ['password', 'remember_token'];

    public $attributes = [
        "level" => 1,
    ];

    public static $rules = [
        "name" => "required",
        "email" => "required|email",
    	"phone" => "required",
    ];

    public function isAdmin() {
        return $this->level == 0;
    }

    public function scopeSearch($query, $value)
    {
        return $query->where("name", "like", "%$value%")
                    ->orWhere("email", "like", "%$value%");
    }

    public function registerRoles(array $roles) {
        $data = [];
        $id = $this->getKey();

        foreach ($roles as $role_id)
            $data[] = ["role_id" => $role_id, "user_id" => $id];

        DB::table("role_user")->where("user_id", $id)->delete();
        DB::table("role_user")->insert($data);
    }

    public function updatePassword($old, $new) {
        if (!Hash::check($old, $this->password))
            return false;

        $this->password = Hash::make($new);
        $this->save();

        return true;
    }
}