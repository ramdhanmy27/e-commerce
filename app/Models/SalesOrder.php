<?php

namespace App\Models;

class SalesOrder extends Model
{
	const CHANNEL_WEB = "SC01";

    protected $connection	=	"pgsql-backend";
    protected $table 		= 	"sales_order";
    protected $primaryKey	= 	"number_order";
    public $timestamps 		= 	false;
}
