<?php

namespace App\Models;

use DB;

class ProductColor extends Model
{
    protected $connection	=	"pgsql-backend";
    protected $table 		= 	"product_color";
    protected $primaryKey	= 	"number_product_color";
    public $incrementing	= 	false;

    static function getNumberProduct($number_product_color){
    	return self::where("number_product_color", $number_product_color)
    				->select("number_product")
    				->firstOrFail()
    				->number_product;
    }

    public function scopeWithCover($query) {
        return $query->addSelect([
            "product_color.number_product", 
            "product_color.number_product_color", 
            DB::raw("product_color.photo[1] as cover")
        ]);
    }
}
