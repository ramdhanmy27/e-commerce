<?php

namespace App\Models;

use DB;

class InventoryItem extends Model
{
    protected $connection	=	"pgsql-backend";
    protected $table 		= 	"inventory_item";
    protected $primaryKey	= 	"sku";
    public $incrementing	= 	false;

    static function getSku($number_product_color,$code_size){
    	return self::where([
    				"number_product_color"=>$number_product_color,
    				"code_size"=>$code_size
    			])
    			->select("sku")

    			//[start] supaya memilih code_warehouse yang qty nya paling banyak
    			->orderBy("qty","desc")
    			->take(1)
    			//[end] supaya memilih code_warehouse yang qty nya paling banyak
    			
    			->first()
    			->sku;
    }

    static function checkStock($sku, $jenis=NULL){
    	$data = self::find($sku)
    			->select("qty")
    			->first()
    			->qty;

    	if ($jenis == NULL || $jenis == FALSE) {
    		return $data;
    	}
        else if ($jenis == TRUE)
            return $data!=0;
    }

    static function getSizeAvailable($number_product, $number_product_color) {
        return self::where([
                    "number_product" => $number_product,
                    "number_product_color" => $number_product_color, 
                ])
                ->select(["code_size", DB::raw("sum(qty) as qty")])
                ->groupBy("code_size")
                ->orderBy("code_size", "asc")
                ->get();
    }

}
