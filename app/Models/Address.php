<?php

namespace App\Models;

use Auth;

class Address extends Model
{
    protected $table = "address";
    protected $fillable = ["name", "phone", "city", "state", "post_code", "address", "primary"];

    protected $attributes = [
    	"primary" => false,
    ];

    public static $rules = [
		"name" => "required",
		"phone" => "required|numeric",
    	"state" => "required",
		"city" => "required",
		"post_code" => "required",
		"address" => "required",
    ];

    public function scopeUser($query) {
        return $query->where("user_id", Auth::user()->id);
    }
}
