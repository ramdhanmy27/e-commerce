<?php

namespace App\Models;

use App\Models\Category;
use App\Models\ProductColor;
use DB;

class Product extends Model
{
    protected $connection = "pgsql-backend";
    protected $table = "product";
    protected $primaryKey = "number_product";
    public $incrementing = false;

    public function scopeLink($query, $link, $id) {
    	return $query->where("pc.number_product_color", $id)
    				->where("product.name", "ilike", str_replace("-", " ", $link));
    }

    public function scopeColor($query) {
        return $query->join("product_color as pc", "product.number_product", "=", "pc.number_product");
    }

    public function scopeSize($query) {
    	return $query->join("product_size as ps", "product.group_size", "=", "ps.group_size");
    }

    /**
     * join stock allocation
     * @required scope color
     * @param  Builder $query
     * @return Builder
     */
    public function scopeWithStock($query) {
        return $query->leftJoin(DB::raw(
            "(select number_product_color, sum(allocation_qty) as qty from stock_allocation _sa 
            group by _sa.number_product_color) as sa"
        ), function($join) {
            $join->on("sa.number_product_color", "=", "pc.number_product_color");
        })->addSelect("sa.qty");
        // return $query->join("stock_allocation as sa", "sa.number_product_color", "=", "pc.number_product_color");
    }

    public function scopeWithCover($query) {
        return $query->color()->addSelect([
            "product.*", 
            "pc.number_product_color", 
            "pc.color", 
            DB::raw("photo[1] as cover"),
            DB::raw("product.name||' '||pc.color as fullname"),
        ]);
    }

    public function category(){
    	return $this->belongsTo(Category::class, "code_category");
    }

    public function productColor(){
    	return $this->belongsTo(ProductColor::class, "number_product");
    }
}
