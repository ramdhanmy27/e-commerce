<?php

namespace App\Models;

class Provinsi extends Model
{
    protected $connection = "pgsql-backend";
    protected $table = "provinsi";
    protected $primaryKey = "pid";
    public $incrementing = false;
}
