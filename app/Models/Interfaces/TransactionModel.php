<?php 

namespace App\Models\Interfaces;

interface TransactionModel {
	const STATUS_DRAFT = 0;
	const STATUS_SENT = 1;
	const STATUS_PROCEED = 2;
}