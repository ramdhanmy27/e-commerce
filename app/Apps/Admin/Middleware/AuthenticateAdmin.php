<?php

namespace App\Apps\Admin\Middleware;

use Closure;
use Auth;

class AuthenticateAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $path = $request->segment(1);

        if (!Auth::guard($guard)->guest()) {
            // redirect to admin
            if (Auth::user()->isAdmin() && $path !== "admin")
                return redirect("admin");
            else
                return $next($request);
        }
        // guest
        else return abort(404);
    }
}
