<?php

namespace App\Apps\Admin\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

class HomeController extends \App\Http\Controllers\Controller
{
	public function getIndex() {
		return view([]);
	}
}
