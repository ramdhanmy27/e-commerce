<?php

namespace App\Apps\Admin\Controllers;

use App\Exceptions\ServiceException;
use App\Http\Requests;
use App\Services\ServiceResponse;
use Illuminate\Http\Request;

class SliderController extends \App\Http\Controllers\Controller
{
	const PATH = "theme/front/img/assets/slider";

	protected $path;

	public function __construct() {
		$this->path = public_path(self::PATH);
	}

	public function getIndex() {
		return view([]);
	}

	public function postUpload(Request $req) {
		$img = $req->file("img");

		if ($img===null || !$img->isValid())
			throw new ServiceException("Uploaded file is invalid");

		$filename = $img->hashName();
		$img->move($this->path, $filename);

		return ["name" => $filename];
	}

	public function postDelete(Request $req) {
		$filename = $req->input("filename");
		$file = $this->path."/".$filename;

		if (!file_exists($file) || !is_writeable($file))
			throw new ServiceException("Failed to delete file");

		@unlink($file);

		return ServiceResponse::make("success", "Photo successfully deleted");
	}

	public function postDeleteAll() {
		foreach ($this->getPhotos() as $file)
			@unlink($file);

		return back();
	}

	public function getPhotos(Request $req = null) {
		$files = glob($this->path."/*.*");

		if ($req===null || !$req->ajax())
			return $files;

		for ($i = count($files)-1; $i >= 0; $i--) { 
			$filename = basename($files[$i]);

			$files[$i] = [
				"name" => $filename,
				"url" => "../".self::PATH."/".$filename,
				"size" => filesize($files[$i]),
			];
		}

		return $files;
	}
}
