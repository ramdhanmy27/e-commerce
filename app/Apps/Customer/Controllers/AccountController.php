<?php

namespace App\Apps\Customer\Controllers;

use App\Exceptions\ValidatorException;
use Auth;
use Flash;
use Illuminate\Http\Request;

class AccountController extends \App\Http\Controllers\Controller
{
	public function getIndex() {
		return view([]);
	}

	public function postIndex(Request $req) {
		Auth::user()->update($req->except("_token"));

		return back();
	}

	public function postPassword(Request $req) {
		$data = $req->except("_token");

		if ($data["confirm"] != $data["new"]) 
			throw new ValidatorException("Konfirmasi password salah");

		if (Auth::user()->updatePassword($data["old"], $data["new"]))
			Flash::success("Password anda telah diganti");
		else
	        Flash::danger("Password tidak valid dengan password saat ini.");

		return back();
	}
}
