<?php

namespace App\Apps\Customer\Controllers;

use App\Exceptions\ValidatorException;
use App\Models\Address;
use Auth;
use Flash;
use Illuminate\Http\Request;

class AddressController extends \App\Http\Controllers\Controller
{
	public function getIndex() {
		$address = Address::user()->get();

		if ($address->count() === 0)
			return $this->getAdd();

		return view([
			"model" => $address,
		]);
	}

    public function getAdd() {
		return view("app.customer.address.form", [
			"model" => Address::class,
		]);
    }

    public function postAdd(Request $req) {
        Address::create($req->except("_token"));
        return redirect("customer/address");
    }

	public function getEdit($id) {
		return view("app.customer.address.form", [
			"model" => Address::findOrFail($id),
		]);
	}

	public function postEdit(Request $req, $id) {
        Address::findOrFail($id)->update($req->except("_token"));
        return redirect("customer/address");
	}

    public function postDelete($id) {
        Address::findOrFail($id)->delete();
		return back();
    }
}
