<?php

Route::group(["middleware" => ["web", "auth.admin"]], function() {
	Route::get("/", function() {
		return view("app.customer.home");
	})->menu("Panel Kontrol Akun", "fa fa-tachometer");

	Route::controller("account", "AccountController")
		->menu("Informasi Akun", "fa fa-user");

	Route::controller("address", "AddressController")
		->menu("Buku Alamat", "fa fa-book");

	Route::controller("sales", "SalesController")
		->menu("Pesanan Saya", "fa fa-shopping-cart");
});