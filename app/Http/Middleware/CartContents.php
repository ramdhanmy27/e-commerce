<?php

namespace App\Http\Middleware;

use Closure;
use Cart;
use Session;

class CartContents
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Cart::count()==0){
            Session::flash("error","You must add item first before proceed to checkout.");
            return redirect()->intended('cart');
        }
        return $next($request);
    }
}
