<?php

namespace App\Http\Middleware;

use Closure;
use Menu;

class Configurator
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $template = null)
    {
        foreach (explode("|", $template) as $config) {
            list($var, $value) = explode("=", $config);

            switch ($var) {
                case "theme":
                    app("theme")->set($value);
                    break;
                
                case "menu":
                    Menu::setDefaultDriver("router");
                    break;
            }
        }

        return $next($request);
    }
}
