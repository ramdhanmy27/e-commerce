<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\InventoryItem as Invent;

class AddCartRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public $max_qty_now = 0;

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $size_available = [];
        $size_available_data = Invent::where("number_product_color", Request::get("number_product_color"))
                                    ->select("code_size")
                                    ->groupBy("code_size")
                                    ->get();
        
        foreach ($size_available_data as $key => $value) 
            $size_available[] = $value->code_size;

        $size_available = implode(",", $size_available);
        $qty_available = [];
        $max_qty_data = Invent::where([
                            "number_product_color" => Request::get("number_product_color"),
                            //[start] apabila semua barang memiliki ukuran , jika tidak semua dikomen saja
                            // "code_size" => Request::get("code_size")
                            //[end] apabila semua barang memiliki ukuran , jika tidak semua dikomen saja
                        ])
                        ->select("qty")->get();

        if (!$max_qty_data->isEmpty()) {
            for ($i=0; $i<$max_qty_data->first()->qty; $i++, $qty_available[]=$i);

            $qty_available = implode(",", $qty_available);
            $this->max_qty_now = $max_qty_data->first()->qty;
        }
        else $qty_available = md5(date("Hisdmy"));
        return [
            "number_product_color" => "required",
            "qty" =>  "required|in:".$qty_available."",
            //[start] apabila semua barang memiliki ukuran
            // "code_size" => "required|in:".$size_available."",
            //[end] apabila semua barang memiliki ukuran
            
            //[start] apabila ada barang yang tidak memiliki ukuran
            "code_size" => $size_available ? "required|in:".$size_available : "in:".$size_available
            //[end] apabila ada barang yang tidak memiliki ukuran
        ];
    }

    public function messages()
    {
        return [
            "number_product_color.required" => "Ups.. terjadi kesalahan. Mohon coba lagi.",
            // "number_product_color.exists" => "The product you choose doesn't exists or out of stock",
            "qty.required" => "Tentukan jumlah barang yang ingin anda beli.",
            "qty.in" => "Produk ini tidak tersedia sebanyak yang anda inginkan. "
                            .($this->max_qty_now == 0 ?: "Hanya $this->max_qty_now barang yang tersedia."),
            "code_size.required" => "Pilih ukuran dari produk yang ingin anda beli.",
            "code_size.in" => "Maaf ukuran yang anda pilih tidak tersedia.",
        ];
    }
}
