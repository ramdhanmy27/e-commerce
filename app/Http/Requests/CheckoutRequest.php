<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CheckoutRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "destination-type"  => "required|in:choose,new",
            "destination-name-choosen"  => "required_if:destination-type,choose",
            // "destination-address-choosen"  => "required_if:destination-type,choose",
            // "destination-city-choosen"  => "required_if:destination-type,choose",
            // "post-code-choosen"  => "required_if:destination-type,choose",
            // "phone-choosen"  => "required_if:destination-type,choose",
            "destination-name-form"  => "required_if:destination-type,new",
            "destination-city-form"  => "required_if:destination-type,new|exists:pgsql-backend.kota,kopid",
            "destination-province-form"  => "required_if:destination-type,new|exists:pgsql-backend.provinsi,pid",
            "destination-address-form"  => "required_if:destination-type,new",
            "phone-form"  => "required_if:destination-type,new|numeric",
            "post-code-form"  => "required_if:destination-type,new|numeric",
        ];
    }

    public function attributes(){
        return [
            "destination-name-form" => "Name",
            "destination-address-form" => "Destination Address",
            "destination-city-form" => "City",
            "destination-province-form" => "Province",
            "phone-form" => "Handphone number",
            "post-code-form" => "Post code",
        ];
    }

    public function messages(){
        return [
            "destination-type.required" => "There is an error. Please try again!",
            "destination-type.in" => "There is an error. Please try again!",
            "destination-name-choosen.required_if" => "You must choose your destination or entry your destination",
            // "destination-address-choosen.required_if" => "You must choose your destination or entry your destination",
            // "destination-city-choosen.required_if" => "You must choose your destination or entry your destination",
            // "post-code-choosen.required_if" => "You must choose your destination or entry your destination",
            // "phone-choosen.required_if" => "You must choose your destination or entry your destination",
            "destination-city-form.exists" => "Destination city you choose doesn't exists!",
            "destination-province-form.exists" => "Destination state you choose doesn't exists!",
            "phone-form.numeric" => "Your phone number must be numeric",
            "post-code-form.numeric" => "Your post code must be numeric",
        ];
    }
}
