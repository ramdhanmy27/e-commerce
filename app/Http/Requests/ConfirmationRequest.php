<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ConfirmationRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "number_order" => "required|exists:pgsql-backend.sales_order,number_order",
            "bank" => "required|exists:pgsql-backend.bank_account,code_bank",
            "transfer_receipt" => "required|image",
            "number_transaction" => "required",
            "transaction_value" => "required|numeric",
        ];
    }

    public function attributes(){
        return [
            "number_order" => "Payment code",
            "bank" => "Bank",
            "transfer_receipt" => "Transfer Receipt",
            "number_transaction" => "Transaction Number",
            "transaction_value" => "Transaction Value",
        ];
    }
}
