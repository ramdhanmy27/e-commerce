<?php

Route::group(['middleware' => ['web']], function () {
	Route::auth();
	Route::get('/', 'HomeController@index');
	Route::controller('payment', "PaymentController");

	/** Shopping Cart */
	Route::post("add-to-cart", "CartController@add");
	Route::post("remove-cart", "CartController@remove");
	Route::post("update-cart", "CartController@updateQty");
	Route::get("clear-cart", "CartController@removeAll");

	Route::get("cart", function() {
		return view("shop.cart");
	});
	Route::group(['middleware'=>['auth','cart']],function(){
		Route::get("checkout","CheckoutController@index");
		Route::post("order","CheckoutController@order");
	});
	Route::post("get-kota/{pid}","CheckoutController@kota");

	/** Products */
	Route::get("search", "ProductController@search");
	Route::get("katalog/{link}", ["uses" => "ProductController@catalog", "as" => "catalog"]);
	Route::get("{link}/{id}", ["uses" => "ProductController@detail", "as" => "product"]);
});