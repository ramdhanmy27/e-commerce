<?php

function to_link($name) {
	return strtolower(str_replace(" ", "-", $name));
}

function product_img_url($id, $color_id, $file, $size = null) {
	if ($size !== null)
		$size .= "-";

	return config("catalog.image.url")."/$id/$color_id/{$size}$file";
}

function product_img($item, $size = null) {
	return product_img_url($item->number_product, $item->number_product_color, $item->cover, $size);
}

function filter_url(array $filter) {
	return url()->current()."?".http_build_query(array_filter($filter + $_GET));
}

function product_img_html($src, $alt, $attr = []) {
	$attribute = "";

	foreach ($attr + ["class" => "img-responsive"] as $key => $val)
		$attribute .= " $key='$val'";

	$default_image = asset(config("catalog.image.default"));

	return "<img alt='$alt' onerror='this.src=\"$default_image\"' src='$src' $attribute>";
}