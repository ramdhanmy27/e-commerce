<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ProductSize;
use DB;

class ProductCatalog
{
	public $query;

	public $filter = [];
	
	public $active_filter = [];

	public $sidebar = [
		"active-filter" => "Filter Aktif",
		"color" => "Warna",
		"price" => "Harga",
		"size" => "Ukuran",
	];

	public function __construct(Request $req, $query) {
		$this->filter = $req->except(["limit", "page"]);

		// Initialize Query
		$query->withCover();

		if (isset($this->filter["q"]))
			$query->where(DB::raw("product.name||' '||pc.color"), "ilike", "%".$this->filter["q"]."%");

		$this->query = clone $query;

		/** Filter */
		$this->filterByColor($query);
		$this->filterBySize($query);
		$this->filterByPrice($query);

		// Active Filter
		if (count($this->active_filter) > 0) {
			view()->composer("product.sidebar.active-filter", function($view) {
				$view->with("filter", $this->active_filter);
			});
		}
		else unset($this->sidebar["active-filter"]);
	}

	protected function filterByColor($query) {
		$color = with(clone $query)->groupBy("pc.color")
					->select("pc.color")->pluck("color");

		if (count($color) > 1) {
			view()->composer("product.sidebar.color", function($view) use ($color) {
				$view->with("colors", $color);
			});
		}
		else unset($this->sidebar["color"]);

		if (isset($this->filter["color"])) {
			$this->query->where("pc.color", $this->filter["color"]);

			$this->active_filter["color"] = [
				"label" => "Warna",
				"value" => $this->filter["color"],
			];
		}
	}

	protected function filterBySize($query) {
		$subquery = with(clone $query)
					->groupBy("product.group_size")
					->select("product.group_size");

		$size = ProductSize::whereRaw("group_size in (".($subquery->toSql()).")")
					->mergeBindings($subquery->getQuery())
					->select(["code_size", "name"])
					->get()->keyBy("code_size")->toArray();

		if (isset($this->filter["size"])) {
			$this->query->size()->where("ps.code_size", $this->filter["size"]);

			$this->active_filter["size"] = [
				"label" => "Ukuran",
				"value" => $size[$this->filter["size"]]["name"],
			];
		}

		if (count($size) > 1) {
			view()->composer("product.sidebar.size", function($view) use ($size) {
					$view->with("size", $size);
			});
		}
		else unset($this->sidebar["size"]);
	}

	protected function filterByPrice($query) {
		view()->composer("product.sidebar.price", function($view) use ($query) {
			$view->with("max_price",
				with(clone $query)
					->select(DB::raw("max(product.basic_price) as price"))
					->value("price")
			);
		});

		// Min Price Query
		$ismin = isset($this->filter["min"]) && is_numeric($this->filter["min"]);

		if ($ismin) {
			$this->query->where("product.basic_price", ">=", $this->filter["min"]);

			$this->active_filter["price"] = [
				"label" => "Harga",
				"value" => "Dari <b>".currency($this->filter["min"])."</b> ",
				"keys" => ["min" => null],
			];
		}

		// Max Price Query
		$ismax = isset($this->filter["max"]) && is_numeric($this->filter["max"]);

		if ($ismax) {
			$this->query->where("product.basic_price", "<=", $this->filter["max"]);

			if (!$ismin) {
				$this->active_filter["price"]["value"] = "";
				$this->active_filter["price"]["keys"] = [];
			}

			$this->active_filter["price"]["label"] = "Harga";
			$this->active_filter["price"]["value"] .= "Sampai <b>".currency($this->filter["max"])."</b>";
			$this->active_filter["price"]["keys"]["max"] = null;
		}
	}

	public function getData() {
		$limitation = config("catalog.limitation");
		$limit = isset($this->filter["limit"]) ? $this->filter["limit"] : $limitation[0];

		view()->composer("front::product.catalog", function($view) use ($limitation) {
			$view->with([
				"sidebar" => $this->sidebar,
				"limitation" => $limitation,
			]);
		});

		return $this->query->withStock()
					->paginate(in_array($limit, $limitation) ? $limit : $limitation[0]);
	}
}
