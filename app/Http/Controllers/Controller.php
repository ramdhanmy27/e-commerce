<?php namespace App\Http\Controllers;

use App\Permission;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * initialize meta data controller 
     * 
     * @param  string  $method  action method
     * @param  array   $param 
     * @return mixed
     */
    public function callAction($method, $param) {
        // build app meta data
        // Generate controller name ID
        $class = get_called_class();

        /** Controller ID */
        // Global Controllers
        preg_match_all("/^App\\\\Http\\\\Controllers\\\\(.+)/i", $class, $match);

        if (count(array_filter($match)) > 0) {
            $pathID = $match[1][0];
        }
        else {
            // Modular Controllers
            preg_match_all("/^App\\\\Apps\\\\(\w+?)\\\\Controllers\\\\(.+)/i", $class, $match);
            $pathID = "App\\".$match[1][0]."\\".$match[2][0];
        }

        /** store value into config */
        config([
            // Controller ID
            "controller.id" => camel2id(preg_replace("/Controller$/i", "", $pathID)),

            // Action method
            "controller.method" => $method,

            // permission
            "controller.permission" => isset($this->permission) ? $this->permission : [],
 
            // Action ID
            "controller.action" => camel2id(preg_replace("/^get/", "", $method)),
        ]);

        // initialize permissions
        if (isset($this->permission))
            app("permission")->initController(config("controller.id"), $this->permission);

        return call_user_func_array([$this, $method], $param);
    }
}