<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use App\Models\ProductColor;
use App\Models\SalesOrder;
use App\Models\StockAllocation;
use DB;
use Illuminate\Http\Request;
use Menu;

class ProductController extends Controller
{
	public function catalog(Request $req, $link) {
		$category = Category::link($link)->firstOrFail();

		return view([
			"title" => $category->name,
			"list" => (new ProductCatalog($req, Product::where("code_category", $category->getKey())))
						->getData(),
		]);
	}

	public function search(Request $req) {
		return view("product.catalog", [
			"title" => "Search Result for '".$req->input("q")."'",
			"list" => (new ProductCatalog($req, Product::query()))->getData(),
		]);
	}
	
	public function detail($link, $id) {
		$product = Product::color()->link($link, $id)
						->select([
							"product.*", "pc.color", "pc.number_product_color", 
							DB::raw("array_to_string(photo, ',') as photo"),
						])
						->firstOrFail();

		$product_id = $product->getKey();
		$category = $product->category;

		Menu::driver("db")->path = [
			["title" => $category->name, "url" => route("catalog", ["link" => to_link($category->name)])],
			["title" => $product->name, "url" => null],
		];

		return view([
			"product" => $product,
			"size" => StockAllocation::where([
		                "number_product" => $product_id,
		                "number_product_color" => $id, 
		                "code_sales_channel" => SalesOrder::CHANNEL_WEB,
		            ])
		            ->select(["code_size", "allocation_qty as qty"])
		            ->orderBy("code_size")
		            ->get(),
			"variation" => ProductColor::where("number_product", $product_id)
								->where("number_product_color", "!=", $id)
								->withCover()->get(),
		]);
	}
}
