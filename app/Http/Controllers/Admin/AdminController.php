<?php 

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\CrudController;
use App\Models\Role;
use App\Models\User;
use App\Models\Model;
use Permission;
use DB;
use Illuminate\Http\Request;
use App\Exceptions\ValidatorException;

class AdminController extends Controller {

    use CrudController;

    /**
     * Init CRUD Trait
     */
    public function __construct() {
        $this->crudInit(function($config) {
            $config->model = User::class;
            $config->table_field = ["name", "email", "created_at", "updated_at"];
            $config->view_field = ["id" => "ID", "name", "email", "created_at", "updated_at"];
            $config->base_url = "admin";
        });
    }

    /**
     * Create data Page
     * 
     * @return View
     */
    public function getAdd() {
        User::$rules += ["password" => "required", "repassword" => "required"];

        return view("layout.crud.add");
    }

    /**
     * Create new User
     * 
     * @param  Request $request
     * @return Response
     */
    public function postAdd(Request $req) {
        $data = $req->except("_token");
        $this->validateUser($data);
        $data["password"] = bcrypt($data["password"]);

        $model = User::create($data);
        $model->registerRoles($req->input("roles", []));

        return redirect("admin");
    }

    /**
     * Edit page
     * 
     * @param  Integer $id
     * @return View
     */
    public function getEdit($id) {
        return view("layout.crud.edit", [
            "model" => User::findOrFail($id),
            "roles" => DB::table("role_user")->where("user_id", $id)->pluck("role_id")
        ]);
    }

    /**
     * Update User
     * 
     * @param  Request $request
     * @return Response
     */
    public function postEdit(Request $req, $id) {
        $data = $req->except("_token");
        $this->validateUser($data);

        // update password if available
        if (is_numeric($data["password"]) || !empty($data["password"])) 
            $data["password"] = bcrypt($data["password"]);
        else
            unset($data["password"]);

        $model = User::find($id);
        $model->update($data);
        $model->registerRoles($req->input("roles", []));

        return redirect("admin");
    }

    /**
     * Validate Data Input
     * 
     * @param  array $data
     * @throw ValidatorException
     */
    private function validateUser(array $data) {
        if ($data["password"] != $data["repassword"])
            throw new ValidatorException("Password tidak sama");

        $validator = Model::makeValidator($data, User::$rules, [], []);

        if ($validator->fails())
            throw new ValidatorException($validator);
    }

	/**
	 * Roles & permissions config
	 * 
	 * @return View
	 */
    public function getPermissions() {
        app("permission")->initAllController(["App\\Http\\Controllers" => "Http/Controllers"]);

        return view([
        	"roles" => Role::pluck("name", "id"),
        	"permissions" => collect(DB::select(
	        	"SELECT p.id, p.display_name, p.description, 
					regexp_replace(name, E'([^\\\\:]+).+', '\\\\1') as \"group\", 
					array_to_string(array_agg(role_id), ',') as roles 
				from permissions p
				left join permission_role pr on pr.permission_id=p.id
				group by p.id, p.display_name, p.name, p.description"
			))->groupBy("group"),
        ]);
    }

    /**
     * Save permission role
     * 
     * @param  Request $req
     * @return Response
     */
    public function postPermissions(Request $req) {
        $data = [];

        foreach ($req->input("perm", []) as $role_id => $permissions) {
            foreach ($permissions as $perm) {
                $data[] = [
                    "permission_id" => $perm,
                    "role_id" => $role_id,
                ];
            }
        }

    	if (count($data) > 0) {
    		DB::transaction(function() use ($data) {
	    		DB::table("permission_role")->delete();
		    	DB::table("permission_role")->insert($data);
    		});
    	}

    	return back();
    }
}