<?php 

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

class HomeController extends Controller {

	public static $permission = [["view", "View Homepage"]];

    public function index() {
        return view("home.index", [
        	"carousel" => [
            	"terbaru" => $this->getProduct(),
            	"termurah" => $this->getProduct(),
            	"terpopuler" => $this->getProduct(),
            ]
        ]);
    }

    private function getProduct() {
    	$list = [];

    	$count = rand(5, 10);
    	$opt = [1,2,3,4,5,6,7,"7-2","7-3",8,9,10,11,12];

    	for ($i = 0; $i < $count; $i++) {
    		$index = array_rand($opt);
    		$list[] = "img/assets/products/product-".$opt[$index].".jpg";
    		unset($opt[$index]);
    	}

    	return $list;
    }
}