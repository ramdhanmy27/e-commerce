<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\CheckoutRequest;

use App\Models\Provinsi;
use App\Models\Kota;
use App\Models\Buyer;
use App\Models\SalesOrder;
use App\Models\OrderItem;
use App\Models\StockAllocation;
use App\Models\InventoryItem as Invent;

use Auth;
use Cart;

class CheckoutController extends Controller
{
    public function index(){
    	$provinsi 	=	Provinsi::all()->keyBy("pid");
    	unset($provinsi["00"]);

    	$buyer_address = Buyer::where("email",Auth::user()->email)
    						->get();
    	
    	return view("shop.checkout")->with([
    			"provinsi"=>$provinsi,
    			"destination"=>$buyer_address
    		]);
    }

    public function order(CheckoutRequest $r){
    	$error = [];
    	foreach (Cart::content() as $key => $value) {
    		$stock = Invent::where("sku",$value->id)
    					->select("qty")
    					->first()
    					->qty;
    		if($value->qty>$stock){
    			$error[$value->id] = $value->name." ".(($value->options->size)?"Size ".$value->options->size:"")." is not available in the quantities you need. ".$stock." remaining in stock for this product";
    		}

    	}
    	if(!empty($error)){
    		return redirect("checkout")->with("error",$error);
    	}

    	if($r->{"destination-type"}=="new"){
    		$buyer = new Buyer();
    		$buyer->name = $r->{"destination-name-form"};
    		$buyer->delivery_address = $r->{"destination-address-form"};
    		$buyer->delivery_city = $r->{"destination-city-form"};
    		$buyer->post_code = $r->{"post-code-form"};
    		$buyer->phone = $r->{"phone-form"};
    		$buyer->email = Auth::user()->email;
    		$buyer->save();

    		$name = $r->{"destination-name-form"};
    		$delivery_address = $r->{"destination-address-form"};
    		$delivery_city = $r->{"destination-city-form"};
    		$post_code = $r->{"post-code-form"};
    		$phone = $r->{"phone-form"};
    		$email = Auth::user()->email;
    		$code_buyer = Buyer::select("code_buyer")
                            ->orderBy("code_buyer","desc")
                            ->first()
                            ->code_buyer;
    	}elseif($r->{"destination-type"}=="choose"){
    		$name = $r->{"destination-name-choosen"};
    		$delivery_address = $r->{"destination-address-choosen"};
    		$delivery_city = $r->{"destination-city-choosen"};
    		$post_code = $r->{"post-code-choosen"};
    		$phone = $r->{"phone-choosen"};
    		$email = Auth::user()->email;
    		$code_buyer = $r->{"code-buyer-choosen"};
    	}

    	$sales_order = new SalesOrder();
    	$sales_order->order_time = date("Y-m-d h:i:s");
    	$sales_order->total_price = str_replace(",", "", Cart::total());
    	$sales_order->total_bill = str_replace(",", "", Cart::total());
    	$sales_order->status = 0;
    	$sales_order->delivery_address = $delivery_address;
    	$sales_order->delivery_city = $delivery_city;
    	$sales_order->post_code = $post_code;
    	$sales_order->payment_deadline = date("Y-m-d h:i:s",strtotime(date("Y-m-d h:i:s")." +1 day"));
    	$sales_order->code_sales_channel = "SC01";
    	$sales_order->code_buyer = $code_buyer;
    	if($sales_order->save()){
    		$loop=0;
    		foreach (Cart::content() as $key => $value) {
    			$data[$loop]["qty"] = $value->qty;
    			$data[$loop]["unit_price"] = $value->price;
    			$data[$loop]["number_order"] = $sales_order->number_order;
    			$data[$loop]["number_product"] = $value->options->number_product;
    			$data[$loop]["number_product_color"] = $value->options->color;
    			$data[$loop]["code_size"] = $value->options->size;
    			$loop++;
    		}
    		if(OrderItem::insert($data)){
    			foreach (Cart::content() as $key => $value) {
    				StockAllocation::where("code_sales_channel","SC01")
    								->where("number_product",$value->options->number_product)
    								->where("code_size",$value->options->size)
    								->where("number_product_color",$value->options->color)
    								->decrement("allocation_qty",$value->qty);
    			}
    		}
    	}
    	Cart::destroy();
    	return view("shop.success")->with("kode_pembayaran",$sales_order->number_order);
    }

    public function kota($pid){
    	return response()->json(Kota::getKota($pid));
    }

}
