<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\ConfirmationRequest;
use App\Models\BankAccount;
use App\Models\BankTransaction;
use App\Models\SalesOrder;
use DB;
use Flash;
use Illuminate\Http\Request;
use File;
use Storage;

class PaymentController extends Controller
{
    public function getConfirmation() {
    	return view([
            "bank" => BankAccount::select(DB::raw("bank_name||' '||account_number as name"), "code_bank")
                        ->get()->keyBy("code_bank")->pluck("name")
        ]);
    }

    public function postConfirmation(Request $req) {
    // public function postConfirmation(ConfirmationRequest $req) {
        DB::beginTransaction();

        try {
            $bank = new BankTransaction();
            $bank->number_transaction = $req->number_transaction;
            $bank->description = "Penjualan produk dengan payment code/number order : ".$req->number_order;
            $bank->is_debet = 1;
            $bank->transaction_value = $req->transaction_value;
            $bank->code_bank = $req->bank;

            if ($bank->saveOrFail()) {
                // $file = $req->transfer_receipt;

                $upload = Storage::disk("backend")->put(
                    "images/confirmation-order/$req->number_order.jpg", 
                    File::get($req->transfer_receipt)
                );

                if ($upload) {
                    $sales = SalesOrder::where("number_order", $req->number_order)->first();
                    $sales->number_transaction = $req->number_transaction;
                    $sales->code_bank = $req->bank;
                    $sales->saveOrFail();
                }
                else throw new \Exception("Maaf terjadi kesalahan! Sistem tidak mengupload file anda.");
            }

            DB::commit();
            Flash::success("Konfirmasi pembayaran anda sudah dikirim.");
        }
        catch(\Exception $e) {
            DB::rollBack();
            dd($e->getMessage(), $req, $req->file('transfer_receipt'));
            Flash::error($e->getMessage());
        }

        return back()->withInput();
    }
}
