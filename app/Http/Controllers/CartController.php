<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//Service
use Cart;
use Input;
use URL;
use Response;

//Request
use App\Http\Requests;
use App\Http\Requests\AddCartRequest;

//Model
use App\Models\InventoryItem as Invent;
use App\Models\ProductColor;
use App\Models\Product;

class CartController extends Controller
{
    public function add(AddCartRequest $cart) {
    	$product = Product::withCover()
                        ->where("pc.number_product_color", $cart->number_product_color)
                        ->firstOrFail();

    	Cart::add(
            Invent::getSku($cart->number_product_color, $cart->code_size),
            $product->fullname,
            $cart->qty,
            $product->basic_price,
            [
    			"size" => $cart->code_size,
    			"color" => $cart->number_product_color,
    			"number_product" => $product->number_product,
                "photo" => $product->cover,
    		]
        );
    	
        $data = [
            "current_qty" => Cart::count(),
            "last_qty" => $cart->qty,
            "total" =>  Cart::total(),
            "cart_content" => [],
        ];

        foreach (Cart::content()->reverse() as $value)
            $data["cart_content"][] = $value;

        return response()->json($data);
    }

    public function removeAll(){
        Cart::destroy();
        return redirect()->intended(URL::previous());
    }

    public function remove(){
        $rowId  =   str_replace("cart", "", Input::get("rowid"));
        Cart::remove($rowId);
        $data["current_qty"]    =   Cart::count();
        $data["total"]          =   Cart::total();
        return response()->json($data);        
    }

    public function updateQty(){
        $rowId  = str_replace("cart", "", Input::get("rowid"));
        $qty    = Invent::checkStock(Cart::get($rowId)->id);
        $status = 200;

        if (Input::get("qty") <= $qty && Input::get("qty")!=0){
            Cart::update($rowId,Input::get("qty"));
            $data["current_qty"] = Cart::count();
            $data["total"] = Cart::total();
        }
        else {
            $data["message"][] = "Produk ini tidak tersedia sebanyak yang anda inginkan. "
                                    .($qty == 0 ?: "Hanya $qty barang yang tersedia.");
            $data["last_qty"] = Cart::get($rowId)->qty;
            $status = 422;
        }

        return response()->json($data, $status);
    }
}
