<?php

namespace App\Providers;

use App\Models\Permission;
use App\Services\Messages;
use App\Services\Theme;
use App\Services\Widget\Grid;
use App\Services\Widget\Menu\Menu;
use App\Services\Widget\Slider;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Enable modular
        $this->modular();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton("theme", function() {
            return new Theme;
        });

        $this->app->singleton("menu", function() {
            return new Menu;
        });

        $this->app->singleton("slider", function() {
            return new Slider;
        });
        
        $this->app->singleton("table.grid", function() {
            return new Grid;
        });
        
        $this->app->singleton("permission", function() {
            return new Permission;
        });
            
        $this->app->singleton("notif", function() {
            return new Messages("notif");
        });
        
        $this->app->singleton("flash", function() {
            return new Messages("flash");
        });
    }

    /**
     * Add Modular Structure
     * 
     * @return void
     */
    public function modular()
    {
        // Get module path
        $path = app_path('Apps\*');

        // List all files in module path
        $apps = glob($path);

        while (list(,$module) = each($apps))
        {
            // Add module's view finder if views exists
            if(is_dir($module.'/views'))
            {
                $this->loadViewsFrom($module.'/views', basename($module));
            }
        }
    }
}
