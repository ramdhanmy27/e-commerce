<?php

namespace App\Providers;

use App\Services\Routing\Router;
use Illuminate\Routing\RoutingServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    public function registerRouter() {
        $this->app['router'] = $this->app->share(function($app) {
            return new Router($app['events'], $app);
        });
    }
}
