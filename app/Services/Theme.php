<?php

namespace App\Services;

use Controller;
use Illuminate\View\Factory;
use Illuminate\View\FileViewFinder;
use View;

class Theme {

    const PATH = "theme/";

	private $theme;
    private $location;

    public function __construct() {
        $this->set(config("view.theme"));
        $this->location = config("view.paths");
    }

    public function isApplied() {
        return $this->theme !== null;
    }

    public function set($theme) {
        // change theme
        $this->theme = $theme;

        // change view finder
        $path = $this->resourcePath("views");

        if (!isset($this->location[$theme])) {
            View::addLocation($path);
            View::addNamespace($theme, $path);

            $this->location[$theme] = $path;
        }
    }

    public function get() {
        return $this->theme;
    }

    public function asset($path, $secure = null) {
        if ($this->isApplied())
            $path = self::PATH.$this->theme."/".$path;

        return app('url')->asset($path, $secure);
    }

    public function view($view = null, $data = [], $merge = []) {
        $factory = app(Factory::class);

        if (func_num_args() === 0 || $view === null)
            return $factory;

        // no view name provided
        if (is_array($view)) {
            $merge = $data;
            $data = is_array($view) ? $view : [];
            $view = $this->defaultPath();
        }

        return $factory->make((!$this->isApplied() ?: $this->theme."::").$view, $data, $merge);
    }

    public function viewExists($path) {
        return file_exists($this->resourcePath("views/".str_replace(".", "/", $path).".blade.php"));
    }

    public function defaultPath($view = null) {
        $path = str_replace("\\", ".", config("controller.id"));

        if ($view === ".")
            return $path;
        else if ($view === null)
            return $path.".".config("controller.action");

        return $path.".$view";
    }

    public function themePath($path = "") {
        return (!$this->isApplied() ?: self::PATH.$this->theme).(empty($path) ?: "/".$path);
    }

    public function resourcePath($path = "") {
        return base_path("resources/".$this->themePath($path));
    }
}