<?php

namespace App\Services\Widget;

interface WidgetContract {

	public function render();
}