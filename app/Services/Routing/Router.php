<?php

namespace App\Services\Routing;

use Closure;
use Menu;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Container\Container;
use App\Services\Routing\Router;
use App\Services\Widget\Menu\MenuRouting;

class Router extends \Illuminate\Routing\Router {

    /**
     * Containing mixed data
     * 
     * @var Object
     */
	private $param;

	public function __construct(Dispatcher $events, Container $container = null) {
        parent::__construct($events, $container);

        $this->routes = new RouteCollection;
        $this->menu = new MenuRouting;
        $this->param = new \StdClass;
    }

    public function menu($attr, Closure $action = null) {
        $driver = Menu::driver("router");

        if (is_array($attr))
            $menu = $driver->addGroup(@$attr["title"], @$attr["url"], @$attr["icon"]);
        else
            $menu = $driver->addGroup($attr);
        
        if (is_callable($action))
            call_user_func($action, $this);
        
        $driver->clearParent();
        return $menu;
    }
	
    /**
     * Add a route to the underlying route collection.
     *
     * @param  array|string  $methods
     * @param  string  $uri
     * @param  \Closure|array|string  $action
     * @return \Illuminate\Routing\Route
     */
    protected function addRoute($methods, $uri, $action)
    {
        return $this->routes->addItem($this->createRoute($methods, $uri, $action));
    }

	/**
     * Create a new Route object.
     *
     * @param  array|string  $methods
     * @param  string  $uri
     * @param  mixed   $action
     * @return \Illuminate\Routing\Route
     */
    protected function newRoute($methods, $uri, $action)
    {
        return (new Route($methods, $uri, $action))
                    ->setRouter($this)
                    ->setContainer($this->container);
    }

    /**
     * Route a controller to a URI with wildcard routing.
     *
     * @param  string  $uri
     * @param  string  $controller
     * @param  array  $names
     * @return void
     *
     * @deprecated since version 5.2.
     */
    public function controller($uri, $controller, $names = []) {
        parent::controller($uri, $controller, $names);
        $this->menu->setPrefix($this->prefix($uri));

        return $this->menu;
    }

    /**
     * Register Administration route
     * 
     * @return MenuRouting
     */
    public function admin() {
        return $this->menu(["title" => "Administrator", "icon" => "fa fa-cogs"], function() {
            $this->group(["prefix" => "admin", "namespace" => "Admin"], function() {
                // Roles
                $this->controller("role", "RoleController")->menu("Roles", "fa fa-user");
 
                // Users, Permissions
                $this->controller("/", "AdminController")
                    ->menu("Users", "fa fa-users")
                    ->menu("Permissions", "fa fa-lock", "permissions");
 
                $this->validateDataId([
                    "edit/{id}/{hashid}",
                    "delete/{id}/{hashid}",
                    "view/{id}/{hashid}",
 
                    "role/edit/{id}/{hashid}",
                    "role/delete/{id}/{hashid}",
                    "role/view/{id}/{hashid}",
                ]);
            });
        });
    }

    public function validateDataId($uri) {
        $this->param->uriValidate = [];

        foreach (is_array($uri) ? $uri : func_get_args() as $url)
            $this->param->uriValidate[] = $this->prefix($url);
    }

    public function getParam() {
        return $this->param;
    }
}