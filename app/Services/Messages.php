<?php

namespace App\Services;

use Cache;

class Messages {

	/**
	 * Messages storage
	 * @var Illuminate\Cache\Repository
	 */
	private $store;

	/**
	 * Address key storage 
	 * @var String
	 */
	private $key;

	public function __construct($key) {
		$this->key = $key;
		$this->store = Cache::store("file");
	}

	public function __call($method, $param) {
		if (!in_array($method, ["info", "danger", "error", "warning", "success"]))
			throw new \BadMethodCallException("Method '$method' not found.");

		$this->push(is_array(current($param)) ? current($param) : $param, $method);
	}

	/**
	 * Push messages into storage
	 * 
	 * @param string|array $messages
	 * @param string $state
	 */
	public function push($messages, $state = "info") {
		// Append messages if stores is not empty
		$data = $this->exists($this->key) ? $this->pull($this->key) : [];

		foreach (is_array($messages) ? $messages : [$messages] as $msg)
			$data[$state][] = $msg;

		$this->store->forever($this->key, $data);
	}

	/**
	 * Check messages existence
	 * 
	 * @param  string $key
	 * @return boolean
	 */
	public function exists() {
		return $this->store->has($this->key);
	}

	/**
	 * Get and remove alert messages
	 * 
	 * @return array
	 */
	public function pull() {
		return $this->store->pull($this->key);
	}
}