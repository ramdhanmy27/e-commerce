#Development Standard

##1. Coding Standard
###a. Penggunaan Case
####Pascal Case
Gunakan huruf besar pada setiap awal kata. Dua kata atau lebih disambung tanpa menggunakan spasi.

- Namespace dan Class
- Folder yang digunakan sebagai namespace
- Nama aplikasi
 
> contoh: `App\Models\HumanResource`

####Camel Case
Hampir sama seperti Pascal Case, hanya saja huruf paling awal menggunakan huruf kecil.

- Nama method (fungsi class) 
 
> contoh: `public function clearCache()`

####Snake Case
Selalu gunakan huruf kecil. Dua kata atau lebih disambung menggunakan underscore.

- Fungsi helper
- Variable / property
- nama Database, table, field
 
> contoh: `function contoh_fungsi()`

####Hyphen
Selalu gunakan huruf kecil. Dua kata atau lebih disambung menggunakan strip / hyphen `-`.

- url
- folder / file view
- javascript, css, image, less/sass
- css class
- html attribute, tag
 
> contoh: `bootstrap-datepicker.js`

###b. Penggunaan library: 
  - pdf : barryvdh/laravel-dompdf
  - image : intervention/image
  - table : datatable (yajra/laravel-datatables-oracle)
  - chart : khill/lavacharts (Google Chart API)

###c. Komentar
Beberapa programmer kadang merasa malas untuk menambahkan komentar pada program yang dibuatnya. Akan tetapi penggunaan komentar pada source code sangat dibutuhkan, apalagi pada source yang sangat panjang. 

Dalam menuliskan komentar, anda tidak perlu menambahkannya pada setiap line baris kode. Cukup pada bagian yang dirasa perlu diberi keterangan saja.

Contoh:

```php
$isZip = false;

// build path for requested files
$requestedFiles = $req->input("path", []);
$requestedFiles = array_filter(array_map(function($item) use (&$isZip) {
          if (!Storage::exists($item))
            return;

          $requestedFiles = Drive::path($item);

          // will create archive file if path is folder
          if (is_dir($requestedFiles))
            $isZip = true;

          return $requestedFiles;
        }, is_array($requestedFiles) ? $requestedFiles : [$requestedFiles]));

$filesCount = count($requestedFiles);
$isZip = $isZip || $filesCount > 1;

// invalid download request
if ($filesCount == 0) 
  throw new ServiceException("Request failed.");

// return zipped file if multiple
if ($isZip) {
  $file = date("YmdHis-").preg_replace("/[\.\s]/", "", microtime()).rand().".zip";
  $filepath = Drive::path($file, "tmp");

  // create archive file
  Zippy::load()->create($filepath, $requestedFiles);

  // get file content and make download response
  $response = response(file_get_contents($filepath))
          ->header("Content-Length", filesize($filepath));

  // delete zip file
  Storage::disk("tmp")->delete($file);

  // return download response
  return $response
      ->header("Content-Description", "File Transfer")
      ->header("Content-Type", "application/octet-stream")
      ->header("Content-Transfer-Encoding", "binary")
      ->header("Expires", "0")
      ->header("Cache-Control", "must-revalidate")
      ->header("Content-Disposition", "attachment; filename=document-".date("Y-m-d").".zip");
}

// return single file to download
return response()->download(current($requestedFiles));
```

####Fungsi & Method
Selalu gunakan komentar pada setiap definisi fungsi. Jelaskan kegunaan fungsi tersebut, tipe data parameter beserta outputnya. 

Contoh:

```php
/**
 * Convert angka ke format currency
 * 
 * @param  integer  $value     
 * @param  integer  $decimals  jumlah angka belakang koma
 * @param  string   $currency  format currency
 * @return string
 */
function currency($value, $decimals=0, $currency="Rp") {
    return "$currency ".number_format($value, $decimals);
}
```

###d. Coding Style
http://administrator.frozennode.com/docs/style-guide#introduction
https://github.com/php-fig/fig-standards


##2. Struktur File

```javascript
.
|-- app
|    |-- Apps
|    |    |-- NamaAplikasi // folder aplikasi
|    |         |-- Controllers
|    |              |-- / NamaController1.php /
|    |              |-- / NamaController2.php /
|    |         |-- Helpers
|    |         |-- Models
|    |       |-- Requests
|    |         |-- views
|    |         |-- / definitions.php /
|    |         |-- / routes.php /
|    |-- Console
|    |-- Events
|    |-- Exceptions
|    |-- Facades
|    |-- Http
|    |    |-- Controllers
|    |    |-- Helpers
|    |    |-- Middleware
|    |    |-- Requests
|    |-- Jobs
|    |-- Libraries
|    |-- Listeners
|    |-- Models
|    |    |-- Interfaces
|    |-- Policies
|    |-- Providers
|    |-- Services
|-- bootstrap
|-- config
|-- database
|    |-- factories
|    |-- migrations
|    |-- seeds
|-- public
|    |-- css
|    |    |-- app
|    |    |-- public
|    |-- fonts
|    |-- img
|    |-- js
|    |    |-- app
|    |    |-- public
|-- resources
|    |-- assets
|    |    |-- css
|    |    |-- js
|    |    |-- less
|    |        |-- app
|    |        |-- public
|    |-- lang
|    |-- views
|         |-- auth
|         |-- errors
|         |-- report
|-- storage
|    |-- app
|    |    |-- public
|    |-- framework
|    |-- logs
|-- tests
|-- vendor
```

##3. Penempatan Source Code
Penempatan source code terbagi atas 2 macam. Ada yang bersifat **modular** dan **global**.

###a. Modular

```javascript
app
 |-- Apps
      |-- NamaAplikasi
           |-- Controllers // Controllers
           |-- Facades // Facades
           |-- Helpers // Fungsi tanpa class / namespace
           |-- Models // Model aplikasi
           |-- Requests // Validasi request 
           |-- Services // Custom library & instance facade
           |-- views // View blade
           |-- definitions.php // Definisi aplikasi, fitur, credential
           |-- routes.php // Route aplikasi
```
Dalam satu project terdapat beberapa aplikasi yang dipisah secara modular. Modul aplikasi ini terdiri atas beberapa folder. Di setiap folder root aplikasi diwajibkan ada file berikut :

- `definitions.php` 
  Berguna sebagai pendefinisian aplikasi beserta fitur dan credential-nya.
- `routes.php`
  Digunakan untuk mendefinisikan route aplikasi.

###b. Global

```javascript
app
 |-- Facades // Facades
 |-- Http
 |    |-- Controllers // Controllers
 |    |-- Helpers // Fungsi tanpa class / namespace
 |    |-- Middleware // Middleware
 |    |-- Requests // Validasi request 
 |    |-- routes.php // Route aplikasi
 |-- Models // Models
 |    |-- Interfaces // Interface Model
 |-- resources
 |    |-- views // View blade
 |-- Services // Custom library & instance facade
```

###c. Javascript & CSS

Script javascript / css yang bersifat general (digunakan di semua aplikasi) dapat disimpan ke dalam file **public/[js/css]/app.[js/css]**. Namun cara ini tidak disarankan, karena file app.[js/css] ini merupakan hasil merge dari beberapa file. Jadi anda akan menambahkan script anda pada line yang berada jauh dibawah.

Jika anda ingin lebih rapih dalam menempatkan source code, berikut penjelasannya

####Javascript

- Jika ingin menambahkan fungsi sederhana yang bersifat seperti helper, tambahkan di file `/resources/assets/js/fn.js`
- Event yang bersifat general (seperti modal atau message alert) tambahkan di file `/resources/assets/js/event.js`
- Apabila ingin menambahkan service, filter atau factory untuk script angular, tambahkan di file `/resources/assets/js/angular.init.js`
- Dan terakhir, script global jQuery tambahkan di file `/resources/assets/js/init.js`

Setelah anda selesai menambahkan source javascript anda, gabungkan dengan tool `gulp` untuk menghasilkan file `app.js`. 

####CSS

Untuk menambahkan script css, disarankan agar menggunakan less. Silahkan lanjut ke bagian Less di bawah.

####Spesifik Aplikasi
#####Modular

Simpan ke **public/[js/css]/app/[nama-aplikasi]/[nama-controlller]/nama-file.[js/css]**

  Contoh: `public/js/app/HR/pegawai/form.js`

#####Global Route 

Simpan ke **public/[js/css]/public/[nama-controlller]/nama-file.[js/css]**

  Contoh: `public/js/public/login/layout.css`


###d. LESS

- Script yang selalu dipanggil di semua halaman (seperti template/tema) masukkan kedalam file **resources/assets/less/all.less**
- Script yang bersifat general (seperti variable path, warna, atau pun class yang digunakan berulang) masukkan kedalam file **resources/assets/less/app.less**.

####Modular
Simpan ke **resources/assets/less/app/[nama-aplikasi]/[nama-controlller]/nama-file.less**

  Contoh: `resources/assets/less/app/HR/pegawai/biodata.less`

    
####Global Route 

Simpan ke **resources/assets/less/public/[nama-controlller]/nama-file.less**

  Contoh: `resources/assets/less/public/login/layout.less`



##4. Penggunaan Source Code
###Javascript & CSS
Pada template blade framework disediakan 2 stack khusus untuk menambahkan script css dan javascript.

####App Stack
Jika anda ingin membuat script khusus yang digunakan untuk beberapa halaman saja, stack ini adalah pilihan yang tepat. Pada stack ini, anda bisa meng-override fungsi-fungsi yang sudah ada ataupun membuat fungsi baru dengan fungsi-fungsi yang sudah disediakan.

```html
push("app-style")
  <link href="{{ asset('css/app/hr/pegawai.css') }}" />
endpush
```

####Dependency Stack
Posisi stack ini berada diantara script dependency wajib dan app.js (script custom yang bersifat general). Oleh karena itu, stack ini digunakan untuk menambahkan script css/javascript yang bersifat sebagai library/dependency. Seperti misalnya anda ingin membuat fungsi yang general yang akan ditambahkan di app.js. Untuk itu anda membutuhkan dependency lagi seperti misalnya moment.js. 

Setelah anda membuatnya, anda pun bisa menggunakannya pada HTML langsung ataupun pada script yang disimpan di stack app.

```html
push("script")
  <script src="{{ asset('js/moment.js') }}">
@endpush

@push("app-script")
  <script src="{{ asset('js/app/hr/pegawai/form.js') }}"></script>
@endpush
```

##5. Pembuatan UI
###Standard UI

- front page aplikasi
- list view data master
  - Table harus menggunakan datatable
  - Default sort bedasarkan label (seperti: nama, Judul)
- editor (create / edit) data master; bisa single atau multiple dataset
- viewer data master
- list view data transaksi 
  - Table harus menggunakan datatable
  - Default sort bedasarkan time
  - Class Model harus mengimplementasi interface ```App\Models\Interfaces\TransactionModel```
- input / edit transaksi
- view transaksi
- slip, yaitu output berupa cetakan sebagai bukti legal dari suatu transaksi
  - Generate Pdf harus menggunakan barryvdh/laravel-dompdf
- report, yaitu hasil query, pengolahan, dan layout dari berbagai data
  - Generate Pdf harus menggunakan barryvdh/laravel-dompdf
- Dashboard
  - Pembuatan chart harus menggunakan khill/lavacharts atau Google Chart

###Generate PDF

- harus menggunakan blade sebagai template layout pdf
- layout blade harus merupakan turunan dari report.layout
- harus menggunakan **barryvdh/laravel-dompdf**


  - Generate Pdf harus menggunakan barryvdh/laravel-dompdf
- Dashboard
  - Pembuatan chart harus menggunakan khill/lavacharts atau Google Chart

###Generate PDF

- harus menggunakan blade sebagai template layout pdf
- layout blade harus merupakan turunan dari report.layout
- harus menggunakan **barryvdh/laravel-dompdf**

