
<section class="toggle active">
	<label>Kategori</label>
	<div class="toggle-content">
		@foreach([] as $color)
			<li><a href="{{ filter_url(["color" => $color]) }}">{{ $color }}</a></li>
		@endforeach
	</div>
</section>

{{-- <h4>Kategori Produk</h4>

<div class="panel panel-default">
	<div class="panel-heading"><b>Kategori</b></div>
	<div class="panel-body list-unstyled">
		@foreach([] as $color)
			<li><a href="{{ filter_url(["color" => $color]) }}">{{ $color }}</a></li>
		@endforeach
	</div>
</div> --}}