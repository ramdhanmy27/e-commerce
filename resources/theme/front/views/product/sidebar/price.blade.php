{!! Form::open(["method" => "GET"]) !!}
	<div class="col-xs-10">
		<div class="form-group input-group">
			{!! Form::number("min", Input::get("min"), ["placeholder" => "Min", "num-format" => ""]) !!}
			<span class="input-group-addon">
				<i class="fa fa-chevron-right"></i>
			</span>
			{!! Form::number("max", Input::get("max"), ["placeholder" => "Max", "num-format" => ""]) !!}
		</div>
	</div>
	<div class="col-xs-2">
		<button class="btn btn-primary" type="submit">
			<i class="fa fa-search"></i>
		</button>
	</div>
{!! Form::close() !!}