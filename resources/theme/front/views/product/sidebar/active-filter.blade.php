<?php $clear_filter = []; ?>

@foreach ($filter as $key => $item)
	<?php $key_exists = isset($item["keys"]); ?>

	<li>
		<i class="glyphicon glyphicon-chevron-right"></i>
		<span tooltip="{{ $item["label"] }}" data-placement="left" class="cursor-default">
			{!! $item["value"] !!}
		</span>

		<a href="{{ filter_url($key_exists ? $item["keys"] : [$key => null]) }}" class="pull-right">
			<i class="fa fa-remove"></i>
		</a>
	</li>

	<?php 
		if ($key_exists) {
			$clear_filter = array_merge($clear_filter, $item["keys"]);
		}
		else $clear_filter[$key] = null;
	?>
@endforeach

<div class="text-right">
	<a href="{{ filter_url($clear_filter) }}" class="text-muted p-md">Clear Filter</a>
</div>
