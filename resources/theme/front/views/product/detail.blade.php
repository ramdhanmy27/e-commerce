@extends("front::app")

@section("title", $product->name)

@push("style")
    <link rel="stylesheet" type="text/css" href="{{ asset("css/public/slider.css") }}" />
	<link rel="stylesheet" type="text/css" href="{{ asset("css/app/product/product.css") }}" />
@endpush

@push("script")
	<script src="{{ asset("js/app/product/cart.js") }}"> </script>
@endpush

@push("app-script")
    <script src="{{ asset("js/public/slider.js") }}"></script>
@endpush

@section("content")
	<div class="row shop">
		<div class="col-md-4">
			<div class="owl-carousel owl-theme" data-plugin-options='{"items": 1, "margin": 10, "loop": false}'>
				@foreach(explode(",", $product->photo) as $item)
					<div> 
						{!! product_img_html(
							product_img_url($product->number_product, $product->number_product_color, $item), 
							$product->name,
							["height" => 300]
						) !!}
					</div>
				@endforeach
			</div>
		</div>

		<div class="col-md-6">
			<div class="summary entry-summary">
				<h1 class="mb-none"><b>{{ $product->name }}</b></h1>
				<h3 class="text-muted mt-sm"><b>{{ $product->color }}</b></h3>

				{{-- Rating & Review 

				<div class="review_num">
					<span class="count" itemprop="ratingCount">2</span> reviews
				</div>

				<div title="Rated 5.00 out of 5" class="star-rating">
					<span style="width:100%">
						<b class="rating">5.00</b> out of 5
					</span>
				</div> --}}

				@if (count($variation) > 0)
					<div class="heading heading-border heading-bottom-border">
						<h5>Variasi Lainnya</h5>
					</div>

					<div class="owl-carousel owl-theme" data-plugin-options='{"items": 8, "margin": 10, "loop": false}'>
						@foreach($variation as $item)
							<?php $name = "$product->name - $item->color"; ?>

							<a href="{{ route("product", [
									"link" => to_link($product->name), 
									"id" => $item->number_product_color
								]) }}">
								{!! product_img_html(
									product_img_url($item->number_product, $item->number_product_color, $item->cover, "small"), 
									$name
								) !!}
							</a>
						@endforeach
					</div>
				@endif

				<div class="tabs tabs-product tabs-secondary">
					<ul class="nav nav-tabs">
						<li class="active">
							<a href="#productDescription" data-toggle="tab">Deskripsi</a>
						</li>
						<li><a href="#productInfo" data-toggle="tab">Informasi Lainnya</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="productDescription">
							<p>{!! $product->description !!}</p>
						</div>

						<div class="tab-pane" id="productInfo">
							{{-- Description --}}
						</div>
					</div>
				</div>
				
				<form enctype="multipart/form-data" method="post" class="cart" action="{{ url('add-to-cart') }}" id="buy-form">
					<input type="hidden" name="_token" value="{{ csrf_token() }}">
					<input type="hidden" name="number_product_color" value="{{ $product->number_product_color}}">

					<div class="panel panel-default">
						<div class="panel-body">
							@include("ui.error")
							<?php $empty = $size->isEmpty(); ?>

							<p id="size-qty" class="pull-right">
								@if ($empty || $size->first()->qty==0)
									<span class="text-danger"> Stock Habis </span>
								@else
									<i class="fa fa-shopping-basket"></i> 
									Tersedia {{ $size->first()->qty }} item
								@endif
							</p>

							<p class="price">
								<span class="amount">{{ currency($product->basic_price) }}</span>
							</p>

							@if (!$empty) 
								<div class="col-md-4 col-sm-4 col-xs-4">
									Jumlah:
									<input type="number" name="qty" class="form-control text-center" 
										value="1" name="quantity" min="1" step="1">
								</div>
								
								<div class="col-md-8 col-sm-8 col-xs-8">
									Ukuran: <br>

									@foreach ($size as $i => $item)
										<label class="btn btn-default btn-borders radio-inline click 
											{{ $i > 0 ?: "btn-primary" }}" for="{{ $item->code_size }}">
											<input type="radio" name="code_size" class="radio-button-item hide" 
												qty="{{ $item->qty }}" id="{{ $item->code_size }}" 
												value="{{ $item->code_size}}" {{ $i > 0 ?: "checked='checked'" }}>
											{{ $item->code_size }}
										</label>
									@endforeach
								</div>
							@endif
						</div>

						@if (!$empty)
							<div class="panel-footer" align="center">
								<a class="btn btn-primary btn-icon" id="buy-button">
									<i class="fa fa-cart-plus"></i>Beli
								</a>
							</div>
						@endif
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection