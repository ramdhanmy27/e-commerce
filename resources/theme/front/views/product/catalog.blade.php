@extends("front::app")

@section("title", $title)

@push("style")
    <link rel="stylesheet" type="text/css" href="{{ asset("css/public/slider.css") }}" />
	<link rel="stylesheet" type="text/css" href="{{ asset("css/app/product/product.css") }}" />
@endpush

@push("app-script")
    <script src="{{ asset("js/public/slider.js") }}"></script>
@endpush

@section("content")
	<div class="row shop">
		<div class="col-md-3">
			<aside class="sidebar">
				{{-- @include("product.sidebar.category") --}}

				<h4>Belanja Bedasarkan</h4>

				<div class="toggle toggle-primary toggle-simple" data-plugin-toggle>
					<div class="panel-group without-bg without-borders">
						@foreach ($sidebar as $id => $title)
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a class="accordion-toggle" data-toggle="collapse" href="#{{ $id }}">
											<b>{{ $title }}</b>
											<i class="fa fa-chevron-down pull-right vmiddle-icon"></i>
										</a>
									</h4>
								</div>
								<div id="{{ $id }}" class="accordion-body collapse in">
									<div class="panel-body list-unstyled">
										@include("product.sidebar.$id")
									</div>
								</div>
							</div>
						@endforeach
					</div>
				</div>
			</aside>
		</div>

		<div class="col-md-9">
			<?php $count = $list->count(); ?>

			@if ($count > 0)
				<div class="row">
					<div class="col-md-6">
						<h2 class="m-none">@yield("title")</h2>

						<?php $start = ($list->currentPage()-1)*$list->perPage(); ?>
						<p>Menampilkan {{ $start+1 }}–{{ $start+$count }} dari {{ $list->total() }} hasil.</p>
					</div>
				</div>

				<ul class="row products product-thumb-info-list" data-plugin-masonry 
					data-plugin-options='{"layoutMode": "fitRows"}'>
					@foreach($list as $item)
						<?php 
							$url = route("product", [
								"link" => title2link($item->name), 
								"id" => $item->number_product_color,
							]);
						?>

						<li class="col-md-4 col-sm-6 col-xs-12 product">
							{{-- <span class="onsale">Sale!</span> --}}
							<span class="product-thumb-info">
								<a href="{{ $url }}">
									<span class="product-thumb-info-image absolute-thumb">
										<span class="product-thumb-info-act">
											<span class="product-thumb-info-act-left">
												<em>View</em>
											</span>
											<span class="product-thumb-info-act-right">
												<em><i class="fa fa-plus"></i> Details</em>
											</span>
										</span>

										{!! product_img_html(
											empty($item->cover) 
												? asset(config("catalog.image.default")) 
												: product_img($item),
											$item->title
										) !!}
									</span>
								</a>
								<span class="product-thumb-info-content">
									<a href="{{ $url }}">
										<h4 class="heading-primary" 
											style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;"
											>{{ $item->name }}</h4>
										<b class="text-muted">{{ $item->color }}</b>

										<span class="price">
											<del><span class="amount">{{ currency($item->basic_price) }}</span></del>
											<ins><span class="amount">{{ currency($item->basic_price) }}</span></ins>
										</span>

										@if ($item->qty == 0)
											<b class="out-of-stock">BARANG HABIS</b>
										@endif
									</a>
								</span>
							</span>
						</li>
					@endforeach
				</ul>
			@else
				<h1>Cannot find product.</h1>
			@endif

			<div class="row">
				<div class="col-md-12 text-right">
					{{ $list->links() }}
				</div>
			</div>
		</div>
	</div>
@endsection