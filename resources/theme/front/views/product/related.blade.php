<h4 class="mb-md text-uppercase">Related <strong>Products</strong></h4>

<div class="row">
	<ul class="products product-thumb-info-list">
		<li class="col-sm-3 col-xs-12 product">
			<a href="shop-product-sidebar.html">
				<span class="onsale">Sale!</span>
			</a>
			<span class="product-thumb-info">
				<a href="shop-cart.html" class="add-to-cart-product">
					<span><i class="fa fa-shopping-cart"></i> Add to Cart</span>
				</a>
				<a href="shop-product-sidebar.html">
					<span class="product-thumb-info-image">
						<span class="product-thumb-info-act">
							<span class="product-thumb-info-act-left"><em>View</em></span>
							<span class="product-thumb-info-act-right"><em><i class="fa fa-plus"></i> Details</em></span>
						</span>
						<img alt="" class="img-responsive" src="{{asset("img/assets/products/product-1.jpg")}}">
					</span>
				</a>
				<span class="product-thumb-info-content">
					<a href="shop-product-sidebar.html">
						<h4>Photo Camera</h4>
						<span class="price">
							<del><span class="amount">$325</span></del>
							<ins><span class="amount">$299</span></ins>
						</span>
					</a>
				</span>
			</span>
		</li>
		<li class="col-sm-3 col-xs-12 product">
			<span class="product-thumb-info">
				<a href="shop-cart.html" class="add-to-cart-product">
					<span><i class="fa fa-shopping-cart"></i> Add to Cart</span>
				</a>
				<a href="shop-product-sidebar.html">
					<span class="product-thumb-info-image">
						<span class="product-thumb-info-act">
							<span class="product-thumb-info-act-left"><em>View</em></span>
							<span class="product-thumb-info-act-right"><em><i class="fa fa-plus"></i> Details</em></span>
						</span>
						<img alt="" class="img-responsive" src="{{asset("img/assets/products/product-2.jpg")}}">
					</span>
				</a>
				<span class="product-thumb-info-content">
					<a href="shop-product-sidebar.html">
						<h4>Golf Bag</h4>
						<span class="price">
							<span class="amount">$72</span>
						</span>
					</a>
				</span>
			</span>
		</li>
		<li class="col-sm-3 col-xs-12 product">
			<span class="product-thumb-info">
				<a href="shop-cart.html" class="add-to-cart-product">
					<span><i class="fa fa-shopping-cart"></i> Add to Cart</span>
				</a>
				<a href="shop-product-sidebar.html">
					<span class="product-thumb-info-image">
						<span class="product-thumb-info-act">
							<span class="product-thumb-info-act-left"><em>View</em></span>
							<span class="product-thumb-info-act-right"><em><i class="fa fa-plus"></i> Details</em></span>
						</span>
						<img alt="" class="img-responsive" src="{{asset("img/assets/products/product-3.jpg")}}">
					</span>
				</a>
				<span class="product-thumb-info-content">
					<a href="shop-product-sidebar.html">
						<h4>Workout</h4>
						<span class="price">
							<span class="amount">$60</span>
						</span>
					</a>
				</span>
			</span>
		</li>
		<li class="col-sm-3 col-xs-12 product">
			<span class="product-thumb-info">
				<a href="shop-cart.html" class="add-to-cart-product">
					<span><i class="fa fa-shopping-cart"></i> Add to Cart</span>
				</a>
				<a href="shop-product-sidebar.html">
					<span class="product-thumb-info-image">
						<span class="product-thumb-info-act">
							<span class="product-thumb-info-act-left"><em>View</em></span>
							<span class="product-thumb-info-act-right"><em><i class="fa fa-plus"></i> Details</em></span>
						</span>
						<img alt="" class="img-responsive" src="{{asset("img/assets/products/product-4.jpg")}}">
					</span>
				</a>
				<span class="product-thumb-info-content">
					<a href="shop-product-sidebar.html">
						<h4>Luxury bag</h4>
						<span class="price">
							<span class="amount">$199</span>
						</span>
					</a>
				</span>
			</span>
		</li>
	</ul>
</div>