@extends("front::app.customer.app")

@section("title", "Buku Alamat")

@section("content.customer")
	@foreach($model as $address)
		<div class="col-md-6">
			<div class="panel panel-primary">
				@if ($address->primary)
					<div class="panel-heading">
						<h3 class="panel-title">
							<i class="fa fa-check"></i> Alamat Utama
						</h3>
					</div>
				@endif

				<div class="panel-body">
					<b>{{ $address->name }}</b> - {{ $address->phone }}<br>
					{{ $address->address }} <br>
					{{ $address->state }} - {{ $address->city }}, {{ $address->post_code }} <br>
				</div>
			</div>
		</div>
	@endforeach
@endsection