<?php 

use App\Models\Address;
use App\Models\Kota;
use App\Models\Provinsi;

?>

@extends("front::app.customer.app")

@section("title", "Tambah Alamat Baru")
@section("input", true)

@section("content.customer")
	{!! Form::model($model, ["url" => "customer/address/add"]) !!}
		<div class="featured-box featured-box-primary align-left mt-xlg">
			<div class="box-content">
				<h4 class="heading-primary mb-md">Informasi Kontak</h4>
				
				{!! Form::group('text', 'name', 'Nama Lengkap') !!}
				{!! Form::group('text', 'phone', 'Telepon / HP') !!}
			</div>
		</div>

		<div class="featured-box featured-box-primary align-left mt-xlg">
			<div class="box-content">
				<h4 class="heading-primary mb-md">Alamat</h4>
				
				{!! Form::group('select', 'provinsi', 'Provinsi', Provinsi::where("pid", "!=", "00")->lists("propinsi", "pid")) !!}
				{!! Form::group('select', 'kota', 'Kota', Kota::lists("kota_cabang", "kopid")) !!}
				{!! Form::group('text', 'post_code', 'Kode Pos') !!}
				{!! Form::group('textarea', 'address', 'Alamat') !!}

				@if (Address::user()->count() === 0) 
					<input type="hidden" name="primary" value="1">
				@else
					<div class="form-group">
						<div class="col-md-offset-3 col-md-9">
							<input type="checkbox" name="primary" id="primary-address" value="1">
							<label for="primary-address">Gunakan sebagai alamat utama</label>
						</div>
					</div>
				@endif

				<div class="form-group">
					<div class="col-md-offset-3 col-md-9">
						{!! Form::submit("Simpan", ["class" => "btn btn-primary"]) !!}
					</div>
				</div>
			</div>
		</div>
	{!! Form::close() !!}
@endsection
