@extends("front::app.customer.app")

@section("content.customer")
	<h4>Halo {{ Auth::user()->name }}!</h4>

	<p>
		Dari Panel Kontrol Akun, Anda dapat melihat detail akun, aktivitas terakhir, dan memperbarui informasi akun Anda.
		<br> Pilih link di bawah ini untuk melihat atau mengubah informasi.
	</p>

	<hr>

	<div class="row">
		<div class="col-sm-6">
			<div class="heading heading-border heading-bottom-border">
				<h3>Informasi Akun</h3>
			</div>
			{{ Auth::user()->email }}
			<a href="#">Ubah Password</a>
		</div>

		<div class="col-sm-6">
			<div class="heading heading-border heading-bottom-border">
				<h3>Buku Alamat</h3>
			</div>
			<a href="#">Atur Alamat</a>
		</div>
	</div>
@endsection