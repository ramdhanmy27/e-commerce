@extends("front::app.customer.app")

@push("script")
    <script src="{{ asset("js/public/slider.js") }}"></script>
@endpush

@push("style")
    <link rel="stylesheet" type="text/css" href="{{ asset("css/public/slider.css") }}" />
@endpush


@section("content.customer")
	<div class="toggle toggle-dark" data-plugin-toggle>
		<section class="toggle active">
			<label>Informasi Akun</label>

			<div class="toggle-content pt-xl pb-xl">
				{!! Form::model(Auth::user()) !!}
					{!! Form::group('text', 'name', 'Name') !!}
					{!! Form::group('text', 'email', 'Email') !!}
					{!! Form::group('text', 'phone', 'Telepon / HP') !!}

					<div class="col-md-offset-3">
						{!! Form::submit("Simpan", ["class" => "btn btn-primary"]) !!}
					</div>
				{!! Form::close() !!}
			</div>
		</section>

		<section class="toggle">
			<label>Ubah Password</label>

			<div class="toggle-content pt-xl pb-xl">
				{!! Form::open(["url" => "customer/account/password"]) !!}
					{!! Form::group('password', 'old', 'Password Sekarang') !!}
					{!! Form::group('password', 'new', 'Password Baru') !!}
					{!! Form::group('password', 'confirm', 'Konfirmasi Password Baru') !!}

					<div class="col-md-offset-3">
						{!! Form::submit("Simpan", ["class" => "btn btn-primary"]) !!}
					</div>
				{!! Form::close() !!}
			</div>
		</section>
	</div>
@endsection