@extends("front::app")

<?php $menu = Menu::driver("router"); ?>

@section("title", $menu->currentExists() ? $menu->getCurrentMenu()->title : "Akun Saya")

@section("content")
	<div class="row shop">
		<div class="col-md-3">
			<ul class="sidebar nav nav-pills nav-stacked">
				@foreach ($menu->getCollection() as $item)
					<li {{ !$item->active ?: "class=active" }}>
						<a href="{{ url($item->url) }}">
							<i class="{{ $item->icon }}"></i> {{ $item->title }}
						</a>
					</li>
				@endforeach

				<li>
					<a href="{{ url("logout") }}">
						<i class="fa fa-sign-out"></i> Logout
					</a>
				</li>
			</ul>
		</div>

		<div class="col-md-9">
			@yield("content.customer")
		</div>
	</div>
@endsection