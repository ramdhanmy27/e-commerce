<?php use App\Models\Role; ?>

{!! Form::model(isset($model) ? $model : $crud->model) !!}
	{!! Form::group('text', 'name', 'Name') !!}
	{!! Form::group('text', 'email', 'Email') !!}
	{!! Form::group('password', 'password', 'Password') !!}
	{!! Form::group('password', 'repassword', 'Confirm Password') !!}
	{!! Form::group('checkboxes', 'roles[]', 'Roles', Role::lists("display_name", "id"), isset($roles) ? $roles : []) !!}

	<div class="col-md-offset-3">
		{!! Form::submit("Simpan", ["class" => "btn btn-primary"]) !!}
		{!! Html::link("admin", "Batal", ["class" => "btn btn-default"]) !!}
	</div>
{!! Form::close() !!}