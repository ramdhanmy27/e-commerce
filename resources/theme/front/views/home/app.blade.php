@extends('front::layout.layout')

@push("script")
    <script src="{{ asset("js/public/slider.js") }}"></script>
@endpush

@push("style")
    <link rel="stylesheet" type="text/css" href="{{ asset("css/public/slider.css") }}" />
@endpush

@section('body')
    {!! app("slider")->render() !!}

    <div class="container">
        <div class="row mt-xlg">
            @yield("content")
        </div>
    </div>
@endsection