<div class="header-nav-main header-nav-main-effect-1 header-nav-main-sub-effect-1 mb-sm mt-xs hidden-sm hidden-xs">
<nav>
    <ul class="nav nav-pills">
        <li class="dropdown dropdown-mega dropdown-mega-shop">
            <a class="dropdown-toggle" href="{{ url("cart") }}">
                <i class="fa fa-shopping-cart"></i> Cart 
                <span class="label label-primary" id="cart-qty">{{ Cart::count() }}</span>
                 - Rp. <span id="cart-price">{{ Cart::total() }}</span>
            </a>

            <ul class="dropdown-menu">
                <div class="dropdown-mega-content">
                    <div style="overflow-y:auto; max-height:350px">
                        <table class="cart" id="cart-content">

                        @if (Cart::count() != 0)
                            @foreach (Cart::content()->reverse() as $cart)

                            <tr rowid="{{ $cart->rowId }}">
                                <td class="product-thumbnail text-center">
                                    {!! product_img_html(
                                        product_img_url(
                                            $cart->options->number_product, 
                                            $cart->options->color, 
                                            $cart->options->photo, 
                                            "small"
                                        ), 
                                        $cart->name,
                                        ["height" => 200]
                                    ) !!}
                                </td>
                                <td class="product-name">
                                    <div class="col-md-12">
                                        {{ $cart->name }}
                                        {{ $cart->options->size ? " - Size ".$cart->options->size : "" }}<br>

                                        <span class="amount">
                                            <strong>{{ currency($cart->price) }}</strong> x 
                                            <span qty-rowid="{{ $cart->rowId }}cart">{{ $cart->qty }}</span> Item
                                        </span>
                                    </div>

                                    <div class="clearCart-content" id="{{ $cart->rowId }}">
                                        <div class="col-md-12">
                                            Anda yakin ingin mengeluarkan barang ini dari keranjang anda ?
                                        </div>
                                        
                                        <hr>

                                        <div class="col-md-12">
                                            <button class="btn btn-primary pull-left removeCart mr-sm" 
                                                onclick="remove_cart(this)">Ya</button>
                                            <button class="btn btn-default pull-left clearCart-no">Tidak</button>
                                        </div>
                                    </div>
                                </td>
                                <td class="product-actions">
                                    <button title="Remove this item" class="clearCart btn btn-primary" 
                                        content-id="{{ $cart->rowId }}">
                                        <i class="fa fa-times"></i>
                                    </button>
                                </td>
                            </tr>
                            @endforeach 
                        @else 
                            <tr>
                                <td align="center">
                                    Keranjang anda masih kosong.
                                </td>
                            </tr>
                        @endif 

                        </table>
                    </div>

                    <table id="action-cart">
                        <tr>
                            <td class="actions" colspan="6">
                                <div class="actions-continue">
                                    
                                    <div class="col-md-6">
                                        <?php $id = md5(date('YmdHis')); ?>

                                        <button content-id="{{ $id }}" class="clearCart btn btn-default">
                                            <i class="fa fa-trash-o"></i> Kosongkan Keranjang
                                        </button>

                                        <div class="clearCart-content" id="{{ $id }}">
                                            <div class="col-md-12">
                                                Anda yakin ingin mengosongkan keranjang belanja anda ?
                                            </div>

                                            <div class="col-md-12">
                                                <button class="btn btn-primary pull-left mr-sm"
                                                    onclick="window.location.href='{{ url('clear-cart')}}'"> Ya </button>
                                                <button class="btn btn-default pull-left clearCart-no"> Tidak </button>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <a href="{{ url("cart") }}">
                                            <button type="submit" class="btn pull-right btn-primary">
                                                Tampilkan Semua <i class="fa fa-angle-right ml-xs"></i>
                                            </button>
                                        </a>
                                    </div>

                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </ul>
        </li>
    </ul>
</nav>
</div>