<!DOCTYPE HTML>
<html>
<head>
    <title> @yield('title', "SAQINA") </title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <link rel="shortcut icon" href="{{ asset("favicon.ico") }}">

    <link rel="stylesheet" href="{{ asset("css/vendor.css") }}">
    @if ($__env->yieldContent("input"))
        <link rel="stylesheet" href="{{ asset("css/input.css") }}">
    @endif

    @stack("style")
    <link rel="stylesheet" href="{{ asset("css/app.css") }}">
    @stack("app-style")
</head>

<body>
    <!-- Header -->
    <header id="header" data-plugin-options='{
        "stickyEnabled": true, 
        "stickyEnableOnBoxed": true, 
        "stickyEnableOnMobile": true, 
        "stickyStartAt": 57, 
        "stickySetTop": "-57px", 
        "stickyChangeLogo": true}'>

        <div class="page-header sub-header-menu">
            <div class="container">
                <ul class="nav nav-pills">
                    <li class="visible-sm visible-xs"> 
                        <a href="{{ url("cart") }}"><i class="fa fa-shopping-cart"></i> Cart</a> 
                    </li>

                    <li> <a href="{{ url("payment/confirmation") }}"><i class="fa fa-money"></i> Konfirmasi Pembayaran</a> </li>

                    @if (Auth::check())
                        @if (Auth::user()->isAdmin())
                            <li> <a href="{{ url("admin") }}"><i class="fa fa-user"></i> Administrator</a> </li>
                        @else
                            <li> <a href="{{ url("customer") }}"><i class="fa fa-user"></i> Akun Saya</a> </li>
                        @endif

                        <li> <a href="{{ url("logout") }}"><i class="fa fa-sign-out"></i> Logout</a> </li>
                    @else
                        <li> <a href="{{ url("login") }}"><i class="fa fa-user"></i> Login / Sign Up</a> </li>
                    @endif
                </ul>
            </div>
        </div>

        <div class="header-body">
            <div class="header-container container">
                <div class="header-row">
                    <div class="header-column">
                        <div class="header-logo">
                            <a href="{{ url("/") }}">
                                <img alt="Porto" width="160" height="44" data-sticky-width="140" 
                                    data-sticky-height="35" data-sticky-top="33" src="{{ asset("logo.gif") }}">
                            </a>
                        </div>
                    </div>
                    <div class="header-column">
                        <div class="header-row">
                            @include("layout.cart")
                            
                            <div class="header-search col-md-5 col-lg-6 col-sm-8 col-xs-6">
                                <form method="get" action="{{ url("search") }}">
                                    <div class="input-group col-xs-12">
                                        <input type="text" class="form-control" name="q" required
                                            placeholder="Cari Produk.." value="{{ Input::get("q") }}">

                                        <span class="input-group-btn">
                                            <button class="btn btn-default" type="submit">
                                                <i class="fa fa-search"></i>
                                            </button>
                                        </span>
                                    </div>
                                </form>
                            </div>

                            {!! Menu::render() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <!-- Body -->
    @yield("body")

    <!-- Footer -->
    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-xs-8">
                    <div class="contact-details">
                        <h4>Kontak Kami</h4>
                        <ul class="contact">
                            <p>
                                <b>Jam layanan Customer Service:</b>
                                <br>Senin - Sabtu jam 08.00 - 17.00 WIB
                            </p>
                            <p> <i class="fa fa-phone"></i> <b>Phone:</b> 0811 830 4578 </p>
                            <p>
                                <i class="fa fa-envelope"></i> 
                                <b>Email:</b> <a href="mailto:mail@example.com">sales@saqina.com</a>
                            </p>
                        </ul>
                    </div>
                </div>

                <div class="col-md-offset-6 col-md-2">
                    <h4>Follow Us</h4>
                    <ul class="social-icons">
                        <li class="social-icons-instagram">
                            <a href="https://instagram.com/saqinadotcom" target="_blank" title="instagram">
                                <i class="fa fa-instagram"></i>
                            </a>
                        </li>
                        <li class="social-icons-facebook">
                            <a href="http://www.facebook.com/saqinaonline" target="_blank" title="Facebook">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                        <li class="social-icons-twitter">
                            <a href="http://www.twitter.com/saqinadotcom" target="_blank" title="Twitter">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>
                        <li class="social-icons-youtube">
                            <a href="https://www.youtube.com/channel/UCs9OPZ34yuaZBrhLYhbYuCA" target="_blank" title="Youtube">
                                <i class="fa fa-youtube"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="footer-copyright">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <p class="text-muted">© Copyright 2016. All Rights Reserved.</p>
                    </div>
                    <div class="col-md-8">
                        <nav>
                            <ul>
                                <li><a href="{{ url("pages/faq") }}">FAQ</a></li>
                                <li><a href="{{ url("pages/contact") }}">Kontak Kami</a></li>
                                <li><a href="{{ url("pages/terms") }}">Persyaratan & Ketentuan</a></li>
                                <li><a href="{{ url("pages/about") }}">Tentang SAQINA</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </footer>

    <!-- Modals -->
    @include("ui.modal", ["id" => "modal-basic"])
    @include("ui.modal", ["id" => "modal-error"])
    @include("ui.modal", [
        "id" => "modal-confirm", 
        "footer" => 
            "<button type='button' data-dismiss='modal' class='btn btn-primary modal-accept'>Ya</button>
            <button type='button' data-dismiss='modal' class='btn btn-default modal-close'>Tidak</button>"
    ])

    <!-- loading image -->
    <div class="hide">
        <img src="{{ asset('img/loading.gif') }}" id="img-loading" />
    </div>

    <!-- Javascript -->
    <script src="{{ asset("js/vendor.js") }}"></script>
    @if ($__env->yieldContent("input"))
        <script src="{{ asset("js/input.js") }}"></script>
    @endif

    @stack("script")
    <script src="{{ asset("js/app.js") }}"> </script>
    <script src="{{ asset("js/public/cart.js") }}"> </script>
    <script type="text/javascript">
        @if(Cart::count()==0)
            $("#action-cart").hide();
        @endif
    </script>
    @stack("app-script")
    <script>
        fn.url.base = '{{ url("/") }}/';

        $(document).ready(function() {
            @if (Notif::exists())
                var notification = {!! json_encode(Notif::pull()) !!};

                for (var type in notification) 
                    fn.notif(notification[type], type);
            @endif
        });
    </script>
    @stack("app-script")
</body>
</html>