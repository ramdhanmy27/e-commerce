<ol class="breadcrumb">
    <li> <a href="{{ url("/") }}"> Home </a> </li>

    <?php $count = count($path); ?>

    @foreach ($path as $i => $menu)
	    <li> 
	    	{{-- Active Breadcrumb--}}
	    	@if ($count === $i+1)
	    		{{ $menu["title"] }}
	    	@else
		    	<a href="{{ $menu["url"]!==null ? url($menu["url"]) : "#" }}"> 
		    		<span>{{ $menu["title"] }}</span>
		    	</a>
	    	@endif
	    </li>
    @endforeach
</ol>
