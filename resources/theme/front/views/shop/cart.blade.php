@extends("front::app")

@section("title","Shopping Cart")

@push("style")
    <link rel="stylesheet" href="{{ asset("css/app/shop/theme-shop.css") }}">
@endpush

@push("script")
	<script src="{{ asset("js/app/product/cart.js") }}"> </script>
@endpush

@section("content")
<div role="main" class="main shop container">
	@if (Cart::count() > 0)
		<div class="featured-boxes">
			<div class="featured-box featured-box-primary align-left mt-sm">
				<div class="box-content">
					<div class="alert alert-danger error-update fade in">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						<span id="message-error">
						</span>
					</div>
					
					<div class="alert alert-success success-update fade in">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						Keranjang anda sudah diperbarui.
					</div>

					@if(Session::has("error"))
						<div class="alert alert-danger fade in">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
							{{Session::get("error")}}
						</div>
					@endif

					<table class="shop_table cart">
						<thead>
							<tr>
								<th class="product-remove">
									&nbsp;
								</th>
								<th class="product-thumbnail">
									&nbsp;
								</th>
								<th class="product-name">
									Produk
								</th>
								<th class="product-price">
									Harga
								</th>
								<th class="product-quantity">
									Jumlah Barang
								</th>
								<th class="product-subtotal">
									Total
								</th>
							</tr>
						</thead>

						<tbody id="cart-content2">
						@forelse (Cart::content()->reverse() as $cart)
							<tr class="cart_table_item" rowid="{{ $cart->rowId }}cart">
								<td class="product-remove">
									<a title="Remove this item" class="remove clearCart cursor-default" 
										content-id="{{ $cart->rowId }}cart">
										<i class="fa fa-times"></i>
									</a>
								</td>
								
								<td class="product-thumbnail">
									{!! product_img_html(
                                        product_img_url(
                                            $cart->options->number_product, 
                                            $cart->options->color, 
                                            $cart->options->photo
                                        ), 
                                        $cart->name,
                                        ["width" => 100, "height" => 100]
                                    ) !!}
								</td>
								
								<td class="product-name">
									{{ $cart->name }} {{ $cart->options->size ? " - Size ".$cart->options->size : "" }}
								</td>
								
								<td class="product-price">
									<span class="amount">{{ currency($cart->price) }}</span>
								</td>
								
								<td class="product-quantity">
									<div class="quantity">
										<input type="number" class="form-control qty-input text-center" 
											price="{{ $cart->price }}" value="{{ $cart->qty }}" min="1" step="1">
									</div>
								</td>

								<td class="product-subtotal">
									<span class="amount" total-rowid="{{ $cart->rowId }}cart">
										{{ currency($cart->price*$cart->qty) }}
									</span>
								</td>
							</tr>

							<tr class="cart_table_item" rowid="{{ $cart->rowId }}cart">
								<td colspan="6" class="clearCart-content cart_table_item" id="{{ $cart->rowId }}cart">
                                    <div class="col-md-12">
                                    	Anda yakin ingin mengeluarkan barang ini dari keranjang anda ?
                                    </div>

                                    <div class="col-md-12">
                                        <font class="btn btn-primary pull-left removeCart mr-sm" 
                                        	onclick="remove_cart(this)">Ya</font>
                                        <font class="btn btn-default pull-left clearCart-no">Tidak</font>
                                    </div>
                                </td>
                            </tr>
						@empty
                            <tr>
                                <td align="center" colspan="6">
                                	Keranjang anda masih kosong
                                </td>
                            </tr>
						@endforelse
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<div class="featured-boxes">
			<div class="row">
					<div class="featured-box featured-box-primary align-left mt-xlg col-sm-6">
						<div class="box-content">
							<h4 class="heading-primary text-uppercase mb-md">Harga Total</h4>
							<table class="cart-totals">

								<tr class="total">
									<th> <b>Total Belanja</b> </th>
									<td> <b><span class="amount" id="subtotal">Rp. {{  Cart::total()  }}</span></b> </td>
								</tr>
							</table>
						</div>
					</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="actions-continue">
					<a href="{{url('checkout')}}" class="btn pull-right btn-primary btn-lg">
						Lanjut ke pembayaran <i class="fa fa-angle-right ml-xs"></i>
					</a>
				</div>
			</div>
		</div>
	@else
		<div class="jumbotron bg-none text-center mt-xl mb-xl">
			<h3 class="pt-xl">
				<i class="fa fa-shopping-basket"></i> Keranjang anda masih kosong
			</h3>
			<p>
				<a href="{{ url("/") }}">
					Lanjutkan belanja <i class="fa fa-angle-right"></i>
				</a>
			</p>
		</div>
	@endif
</div>
@endsection