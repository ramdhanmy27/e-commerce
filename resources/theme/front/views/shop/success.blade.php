@extends("front::app")

@section("title","Transaction Succeed")

@section("content")
<div role="main" class="main shop">
	<center>
		<div class="col-md-12">
			<div class="alert alert-success">
				Transaction Succeed! Your payment code is <b>{{$kode_pembayaran}}</b>
			</div>
		</div>
	<center>
</div>
@endsection