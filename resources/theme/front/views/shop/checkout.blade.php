@extends("front::app")

@section("title","Checkout")

@push("style")
    <link rel="stylesheet" href="{{  asset("css/app/shop/theme-shop.css")  }}">
@endpush

@push("script")
	<script src="{{  asset("js/app/product/checkout.js")  }}"> </script>
@endpush

@section("content")
<div role="main" class="main shop">

	<div class="container">

		<div class="row">
			<div class="col-md-12">
				@if(session('error'))
					<div class="alert alert-danger">
						@foreach(session('error') as $e)
							<li>{{$e}}</li>
						@endforeach
					</div>
				@endif
				<hr class="tall">
			</div>
		</div>

		<div class="row">
			<div class="col-md-9">

				<div class="panel-group" id="accordion">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
									Destination Address
								</a>
							</h4>
						</div>
						<div id="collapseTwo" class="accordion-body collapse {{(session('error'))?"":"in"}}">
							<div class="panel-body">
								<form action="order" name="formOrder" method="post">
									<input type="hidden" name="destination-type" value="choose">
									<input type="hidden" name="destination-name-choosen">
									<input type="hidden" name="destination-address-choosen">
									<input type="hidden" name="destination-city-choosen">
									<input type="hidden" name="post-code-choosen">
									<input type="hidden" name="phone-choosen">
									<input type="hidden" name="code-buyer-choosen">
									<input type="hidden" name="_token" value="{{csrf_token()}}">
									<div class="row">
										<div class="col-md-12">
											<div class="tabs">
												<ul class="nav nav-tabs">
													<li class="active">
														<a href="#tab-choose" data-toggle="tab"><i class="fa fa-map-marker"></i> Choose destination</a>
													</li>
													<li>
														<a href="#tab-new" data-toggle="tab"><i class="fa fa-plus"></i> Add new destination</a>
													</li>
												</ul>
												<div class="tab-content">
													<div id="tab-choose" class="tab-pane active" style="overflow-y:scroll;max-height:400px">
														@forelse($destination as $d)
															<div class="panel panel-default destination-choice" destination-name="{{$d->name}}" destination-address="{{$d->delivery_address}}" destination-city="{{$d->delivery_city}}" post-code="{{$d->post_code}}" phone="{{$d->phone}}" code-buyer="{{$d->code_buyer}}" style="cursor:pointer">
																<div class="panel-body" style="max-height:130px;overflow-y:scroll">
																	<div class="col-md-3">
																		<h4>
																			<span class="label label-default" style="word-wrap: break-word; white-space: normal;">
																				{{$d->name}}
																			</span>
																		</h4>
																	</div>
																	<div class="col-md-9">
																		{{$d->delivery_address}}<br>
																		{{$d->delivery_city}}<br>
																		{{$d->post_code}}<br>
																		Phone : {{$d->phone}}<br>
																	</div>
																</div>
															</div>
														@empty
															<center>You have not added any address. Please add new destination!</center>
														@endforelse
													</div>
													<div id="tab-new" class="tab-pane">
														<div class="row">
															<div class="form-group">
																<div class="col-md-6">
																	<label>Province</label>
																	<select class="form-control" id="destination-province" name="destination-province-form">
																		<option value="00">Select a province</option>
																		@forelse($provinsi->sortBy("propinsi") as $p)
																			<option value="{{$p->pid}}">{{$p->propinsi}}</option>
																		@empty
																		@endforelse
																	</select>
																</div>
																<div class="col-md-6">
																	<label>City</label>
																	<select class="form-control" id="destination-city" name="destination-city-form">
																		<option value="">Select a city</option>
																	</select>
																</div>
															</div>
														</div>
														<div class="row">
															<div class="form-group">
																<div class="col-md-12">
																	<label>Name</label>
																	<input type="text" value="{{old('destination-name-form')}}" class="form-control" name="destination-name-form">
																</div>
															</div>
														</div>
														<div class="row">
															<div class="form-group">
																<div class="col-md-12">
																	<label>Destination Address</label>
																	<input type="text" value="{{old('destination-address-form')}}" class="form-control" name="destination-address-form">
																</div>
															</div>
														</div>
														<div class="row">
															<div class="form-group">
																<div class="col-md-12">
																	<label>Handphone Number</label>
																	<input type="number" value="{{old('phone-form')}}" class="form-control" name="phone-form">
																</div>
															</div>
														</div>
														<div class="row">
															<div class="form-group">
																<div class="col-md-12">
																	<label>Post Code</label>
																	<input type="number" value="{{old('post-code-form')}}" class="form-control" name="post-code-form">
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
							</div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
									Review & Payment
								</a>
							</h4>
						</div>
						<div id="collapseThree" class="accordion-body collapse {{(session('error'))?"in":""}}">
							<div class="panel-body">
								<table class="shop_table cart">
									<thead>
										<tr>
											<th class="product-thumbnail">
												&nbsp;
											</th>
											<th class="product-name">
												Product
											</th>
											<th class="product-price">
												Price
											</th>
											<th class="product-quantity">
												Quantity
											</th>
											<th class="product-subtotal">
												Total
											</th>
										</tr>
									</thead>
									<tbody>
										@forelse(Cart::content()->reverse() as $cart)
										<tr class="cart_table_item {{(session('error') && isset(session('error')[$cart->id]))?"danger":""}}">
											<td class="product-thumbnail">
												{!! product_img_html(
			                                        product_img_url(
			                                            $cart->options->number_product, 
			                                            $cart->options->color, 
			                                            $cart->options->photo
			                                        ), 
			                                        $cart->name,
			                                        ["width" => 100, "height" => 100]
			                                    ) !!}
											</td>
											
											<td class="product-name">
												{{ $cart->name }} {{ $cart->options->size ? " - Size ".$cart->options->size : "" }}
											</td>
											
											<td class="product-price">
												<span class="amount">{{ currency($cart->price) }}</span>
											</td>
											
											<td class="product-quantity">
												<div class="quantity">
													{{ $cart->qty }}
												</div>
											</td>

											<td class="product-subtotal">
												<span class="amount" total-rowid="{{ $cart->rowId }}cart">
													{{ currency($cart->price*$cart->qty) }}
												</span>
											</td>
										</tr>
										@empty
										<tr class="cart_table_item">
				                            <td align="center" colspan="5">
				                                You have not added any product to cart.
				                            </td>
				                         </tr>
										@endforelse
									</tbody>
								</table>

								<hr class="tall">

								<h4 class="heading-primary">Cart Totals</h4>
								<table class="cart-totals">
									<tbody>
										<tr class="total">
											<th>
												<strong>Order Total</strong>
											</th>
											<td>
												<strong><span class="amount">{{Cart::total()}}</span></strong>
											</td>
										</tr>
									</tbody>
								</table>

								<hr class="tall">

								<h4 class="heading-primary">Payment</h4>
									<div class="row">
										<div class="col-md-12">
											<span class="remember-box checkbox">
												<label>
													<input type="checkbox" checked="checked">Direct Bank Transfer
												</label>
											</span>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<span class="remember-box checkbox">
												<label>
													<input type="checkbox">Cheque Payment
												</label>
											</span>
										</div>
									</div>
									<div class="row">
										<div class="col-md-12">
											<span class="remember-box checkbox">
												<label>
													<input type="checkbox">Paypal
												</label>
											</span>
										</div>
									</div>
							</div>
						</div>
					</div>
				</div>

				<div class="actions-continue">
					<input type="submit" value="Place Order" name="proceed" class="btn btn-lg btn-primary mt-xl">
				</div>
				</form>
			</div>
			<div class="col-md-3">
				<h4 class="heading-primary">Cart Totals</h4>
				<table class="cart-totals">
					<tbody>
						<tr class="total">
							<th>
								<strong>Order Total</strong>
							</th>
							<td>
								<strong><span class="amount">{{Cart::total()}}</span></strong>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>

	</div>

</div>
@endsection