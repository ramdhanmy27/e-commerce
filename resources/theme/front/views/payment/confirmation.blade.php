@extends("front::app")

@section("title", "Konfirmasi Pembayaran")

@section("content")
	@if(session('success'))
		<div class="alert alert-success">
			{{session('success')}}
		</div>
	@endif
	
	{!! Form::open(["files"=>true]) !!}
		{!! Form::group('text', 'number_order', 'Payment Code') !!}
		{!! Form::group('select', 'bank', 'Bank', $bank) !!}
		{!! Form::group('file', 'transfer_receipt', 'Transfer Receipt',["accept"=>"image/*"]) !!}
		{!! Form::group('text', 'number_transaction', 'Transaction Number') !!}
		{!! Form::group('text', 'transaction_value', 'Transaction Value (IDR)') !!}

		<div class="col-md-offset-3">
			{!! Form::submit("Konfirmasi", ["class" => "btn btn-primary"]) !!}
		</div>
	{!! Form::close() !!}
@endsection