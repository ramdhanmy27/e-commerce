<!doctype html>
<html class="fixed">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

		<title>@yield("title")</title>
	    <link rel="stylesheet" href="{{ asset("css/vendor.css") }}">
	    <link rel="stylesheet" href="{{ asset("css/app.css") }}">
	</head>
	<body>
		<section class="body-sign">
			<div class="center-sign">
				<a href="/" class="logo pull-left">
					<img src="{{ asset("img/logo.png") }}" height="54" alt="Porto Admin" />
				</a>

				<div class="panel panel-sign">
					<div class="panel-title-sign mt-xl text-right">
						<h2 class="title text-uppercase text-bold m-none"><i class="fa fa-user mr-xs"></i> @yield("title")</h2>
					</div>
					<div class="panel-body">
						@yield("content")
					</div>
				</div>

				<p class="text-center text-muted mt-md mb-md">&copy; Copyright {{ date("Y") }}. All Rights Reserved.</p>
			</div>
		</section>
	</body>
</html>