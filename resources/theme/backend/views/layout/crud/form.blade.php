{!! Form::model(isset($model) ? $model : $crud->model) !!}
	@foreach ($crud->form as $item)
		{!! call_user_func_array([Form::class, "group"], $item) !!}
	@endforeach
	
	<div class="col-md-offset-3">
		{!! Form::submit("Simpan", ["class" => "btn btn-primary"]) !!}
		{!! Html::link($crud->base_url, "Batal", ["class" => "btn btn-default"]) !!}
	</div>
{!! Form::close() !!}