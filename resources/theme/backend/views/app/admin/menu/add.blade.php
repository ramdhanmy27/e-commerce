@extends("backend::layout.panel")

@section("input", true)
@section("title", "Create Menu")

@section("body")
	@include("app.admin.menu.form", [
		"model" => $model,
	])
@endsection

@push("app-script")
	<script src="{{ asset("js/app/admin/menu-category.js") }}"></script>
@endpush