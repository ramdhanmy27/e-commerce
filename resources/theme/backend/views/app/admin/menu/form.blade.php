<?php

use App\Models\Category;
use App\Models\Menu;

?>

{{ Form::model($model) }}
	{!! Form::group('select', 'category', "Category", Category::lists("name", "name")) !!}

	{!! Form::group('text', 'title') !!}
	{!! Form::group('text', 'url', "URL") !!}
	{!! Form::group('select', 'parent', "Parent", Menu::lists("title", "id")) !!}
	{!! Form::group('checkbox', 'enable') !!}

	<div class="col-md-offset-3 col-md-9">
		{!! Form::submit("Submit") !!}
	</div>
{{ Form::close() }}

