var elixir = require('laravel-elixir');

elixir.config.sourcemaps = false;

var tmp = {};
var pub = {};

var shared = {
    js: "./resources/assets/js/",
    css: "./resources/assets/css/",
};

var themelixir = function(theme, callback) {
    if (theme !== "")
        theme = "theme/"+ theme +"/";

    elixir.config.assetsPath = "resources/"+ theme +"/assets/";

    tmp = {
        js: "./resources/"+ theme +"assets/js/tmp/",
        css: "./resources/"+ theme +"assets/css/tmp/",
    };

    pub = {
        js: "./public/"+ theme +"js/",
        css: "./public/"+ theme +"css/",
    }

    elixir(callback);
}

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

themelixir("front", function(mix) {
    mix

        //////////
        // LESS //
        //////////
        
        .less('all.less', tmp.css + "all.css")
        
        /////////
        // CSS //
        /////////

        // application theme
        .styles([
            "theme/theme.css",
            "theme/theme-elements.css",
            "theme/skin.css",

            // all less
            tmp.css + "all.css",
        ], pub.css + "app.css")
        
        // vendor
        .styles([
            shared.css + "bootstrap.min.css",
            shared.css + "font-awesome.min.css",
            "ui/pnotify.custom.css",
        ], pub.css + "vendor.css")

        // form input
        .styles([
            shared.css + "input/datepicker3.css",
            shared.css + "input/bootstrap-timepicker.min.css",
            shared.css + "input/dropzone/dropzone.css",
            shared.css + "input/bootstrap-fileupload.min.css",
            shared.css + "input/select2.css",
        ], pub.css + "input.css")

        // slider
        .styles([
            "ui/rs-plugin/settings.css",
            "ui/owl.carousel.min.css",
            "ui/owl.theme.default.min.css",
        ], pub.css + "public/slider.css")

        // product theme - shop
        .styles([
            "theme/theme-blog.css",
            "theme/theme-shop.css",
        ], pub.css + "app/product/product.css")

        ////////////////
        // Javascript //
        ////////////////

        // application theme
        .scripts([
            "theme/theme.js",

            // App JS
            shared.js + "fn.js",
            shared.js + "angular.init.js",
            "app/event.js",
            "app/init.js",
        ], pub.js + "app.js")
        
        // vendor
        .scripts([
            shared.js + "jquery.min.js",
            shared.js + "bootstrap.min.js",
            // shared.js + "angular.min.js",
            "theme/modernizr.js",
            "ui/pnotify.custom.js",
        ], pub.js + "vendor.js")

        // form input
        .scripts([
            shared.js + "input/validator.min.js",
            shared.js + "input/bootstrap-maxlength.js",
            shared.js + "input/bootstrap-datepicker.js",
            shared.js + "input/bootstrap-timepicker.js",
            shared.js + "input/bootstrap-fileupload.min.js",
            shared.js + "input/dropzone.min.js",
            shared.js + "input/select2.min.js",
            shared.js + "input/ios7-switch.js",
        ], pub.js + "input.js")

        // slider
        .scripts([
            "ui/jquery.appear.min.js",
            "ui/common.min.js",
            "ui/rs-plugin/jquery.themepunch.tools.min.js",
            "ui/rs-plugin/jquery.themepunch.revolution.min.js",
            "ui/owl.carousel.min.js",
        ], pub.js + "public/slider.js")
});

themelixir("backend", function(mix) {
    mix

        //////////
        // LESS //
        //////////
        
        .less('all.less', tmp.css + "all.css")
        
        /////////
        // CSS //
        /////////

        // application theme
        .styles([
            "theme/theme.css",
            "theme/skin.css",

            // all less
            tmp.css + "all.css",
        ], pub.css + "app.css")
        
        // vendor
        .styles([
            shared.css + "bootstrap.min.css",
            shared.css + "font-awesome.min.css",
            "ui/jquery.dataTables.min.css",
            "ui/datatables.css",
            "ui/pnotify.custom.css",
        ], pub.css + "vendor.css")

        // form input
        .styles([
            shared.css + "input/datepicker3.css",
            shared.css + "input/bootstrap-timepicker.min.css",
            shared.css + "input/dropzone/dropzone.css",
            shared.css + "input/bootstrap-fileupload.min.css",
            shared.css + "input/select2.css",
        ], pub.css + "input.css")

        ////////////////
        // Javascript //
        ////////////////

        // application theme
        .scripts([
            "ui/jquery.dataTables.min.js",
            "theme/theme.js",

            // App JS
            shared.js + "fn.js",
            shared.js + "angular.init.js",
            "app/event.js",
            "app/init.js",
        ], pub.js + "app.js")
        
        // vendor
        .scripts([
            shared.js + "jquery.min.js",
            shared.js + "bootstrap.min.js",
            // shared.js + "angular.min.js",
            "theme/nanoscroller.js",
            "theme/modernizr.js",
            "ui/pnotify.custom.js",
        ], pub.js + "vendor.js")

        // form input
        .scripts([
            shared.js + "input/validator.min.js",
            shared.js + "input/bootstrap-maxlength.js",
            shared.js + "input/bootstrap-datepicker.js",
            shared.js + "input/bootstrap-timepicker.js",
            shared.js + "input/bootstrap-fileupload.min.js",
            shared.js + "input/dropzone.min.js",
            shared.js + "input/select2.min.js",
            shared.js + "input/ios7-switch.js",
        ], pub.js + "input.js")
});
