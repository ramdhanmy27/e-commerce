var size = $("[name='code_size']");

size.change(function(){
	var checked = $("[name='code_size']:checked");
	var value = checked.val();
	var qty = fn.int(checked.attr("qty"));

	size.each(function(){
		$("[for='"+$(this).val()+"']").removeClass("btn-primary").addClass("btn-default");
	});

	$("[for='"+value+"']").addClass("btn-primary").removeClass("btn-default");
	$("#size-qty").html(
		qty=="0" 
			? "<span class='text-danger'>Stock Habis</span>" 
			: "<i class='fa fa-shopping-basket'></i> Tersedia "+ qty +" item"
	);
});

$("#buy-button").click(function(){
	$.ajax({
		url: fn.url("add-to-cart"),
		type: "POST",
		data: $("#buy-form").serialize(),
		success: function(data){
			$("#cart-qty").html(data.current_qty);

			if (data.current_qty==0) 
                $("#action-cart").fadeOut();
			else
                $("#action-cart").fadeIn();

			$("#cart-price").html(data.total);
			$("#cart-content").empty();

			var link = fn.url("clear-cart");

			$.each(data.cart_content, function(key, value) {
                $("#cart-content").append(
                	'<tr rowid="'+value.rowId+'">'+
                        '<td class="product-thumbnail">'+
                            ' <img width="100" height="100" alt="" class="img-responsive" src="">'+
                        '</td>'+
                        '<td class="product-name">'+
                            '<div class="col-md-12">'+
                                value.name + (value.options.size ? ' - Size '+ value.options.size: '') + '<br>' +
                                '<span class="amount">'+
                                	'<strong>Rp. '+fn.format.num(value.price)+'</strong> x ' +
                                	'<span qty-rowid="'+value.rowId+'cart">'+value.qty+'</span> Item'+
                                '</span>'+
                            '</div>'+
                            '<div class="clearCart-content" id="'+value.rowId+'">'+
                                '<div class="col-md-12">'+
                                    'Anda yakin ingin menghapus barang ini dari keranjang anda ?'+
                                '</div> <hr>'+
                                '<div class="col-md-12">'+
                                    '<button class="btn btn-primary pull-left removeCart mr-sm" onclick="remove_cart(this)">Yes</button>'+
                                    '<button class="btn btn-default pull-left clearCart-no"  onclick="clear_cart_no(this)">No</button>'+
                                '</div>'+
                            '</div>'+
                        '</td>'+
                        '<td class="product-actions">'+
                            '<button title="Remove this item" class="clearCart btn btn-primary" content-id="'+value.rowId+'" onclick="clear_cart(this)">'+
                                '<i class="fa fa-times"></i>'+
                            '</button>'+
                        '</td>'+
                    '</tr>'
                );

	            $(".clearCart-content").hide();
			});
			
			fn.notif("Produk ditambahkan ke keranjang belanja", "success");
		},
		error: function(data) {
			fn.alert(data.responseJSON, "Peringatan!");
		}
	});
});

$(".success-update").hide();
$(".error-update").hide();

$(".qty-input").change(function(){
	var $elem = $(this);
	var rowid = $elem.parent().parent().parent().attr("rowid");
	var qty = $elem.val();
	var price = $elem.attr("price");

	$.ajax({
		url: fn.url("update-cart"),
		type: 'POST',
		data: {rowid: rowid, qty: qty},
		success: function(data){
			$("[qty-rowid='"+rowid+"']").text(qty);
			$("#cart-qty").html(data.current_qty);
			$("#subtotal").html(data.total);
            $("#cart-price").html(data.total);
            $("[total-rowid='"+rowid+"']").html("Rp. " +fn.format.num(parseInt(qty) * parseInt(price)));

            $(".error-update").fadeOut(function(){
                $(".success-update").fadeIn();
            });
		},
		error: function(data){
			$("#message-error").empty();

			$.each(data.responseJSON.message, function(key, value){
				$("#message-error").append("<li>"+value+"</li>");
			});

            $(".success-update").fadeOut(function(){
                $(".error-update").fadeIn();
            });

            $elem.val(data.responseJSON.last_qty);
		}
	});
});

