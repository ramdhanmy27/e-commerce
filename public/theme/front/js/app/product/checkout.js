$("#destination-province").change(function(){
	var pid = $(this).val();
	$.ajax({
		url 	: fn.url("get-kota/"+pid),
		method 	: "POST",
		success : function(data){
			$("#destination-city").empty();
			$.each(data,function(index,value){
				$("#destination-city").append("<option value='"+value.kopid+"'>"+value.kota_cabang+"</option>");
			});
		}
	});
});

$(".destination-choice").click(function(){
	$(".destination-choice").each(function(){
		$(this).removeClass("btn-primary");
	});
	$(this).addClass("btn-primary");
	$("[name='destination-name-choosen']").val($(this).attr("destination-name"));
	$("[name='destination-address-choosen']").val($(this).attr("destination-address"));
	$("[name='destination-city-choosen']").val($(this).attr("destination-city"));
	$("[name='post-code-choosen']").val($(this).attr("post-code"));
	$("[name='phone-choosen']").val($(this).attr("phone"));
	$("[name='code-buyer-choosen']").val($(this).attr("code-buyer"));
});

$("#tab-choose").click(function(){
	$("[name='destination-type']").val("choose");
});

$("#tab-new").click(function(){
	$("[name='destination-type']").val("new");
});
