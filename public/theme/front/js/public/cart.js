function clear_cart(thisitem){
	$(thisitem).fadeOut("fast",function(){
		var target = $(thisitem).attr("content-id");
		$("#"+target).fadeIn("fast");
	});
}

function clear_cart_no(thisitem){
	var target = $(thisitem).parent().parent().attr("id");
	
	$("#"+target).fadeOut("fast",function(){
		$("[content-id='"+target+"']").fadeIn("fast");
	}); 
}

function remove_cart(thisitem){
	var target = $(thisitem).parent().parent().attr("id");

	$.ajax({
		url     :   fn.url("remove-cart"),
		type    :   'POST',
		data    :   {rowid:target},
		success :   function(data){
			$("[rowid='"+target+"']").fadeOut();
			target  =   target.replace("cart","");
			var check = 0;
			if($("[rowid='"+target+"']").length){
				check=1;
				$("[rowid='"+target+"']").fadeOut();
			}
			target2  =   target+"cart";
			if($("[rowid='"+target2+"']").length){
				$("[rowid='"+target2+"']").fadeOut();
			}

			$("#cart-qty").html(data.current_qty);
			$("#cart-price").html(data.total);
			$("#subtotal").html(data.total);
			if(data.current_qty===0){
				var tr = '<tr>'+
							'<td align="center">'+
								'You have not added any product to cart.'+
							'</td>'+
						'</tr>';
				$("#cart-content").append(tr);
				if(check===1){
					var tr = '<tr>'+
								'<td align="center" colspan="6">'+
									'You have not added any product to cart.'+
								'</td>'+
							'</tr>';
					$("#cart-content2").append(tr);
				}
                $("#action-cart").fadeOut();
			}
		}
	});
}

$(".clearCart-content").hide();

$(".clearCart").click(function(){
    clear_cart(this);
});

$(".clearCart-no").click(function(){
    clear_cart_no(this);
}); 