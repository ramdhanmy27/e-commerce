$(document).ready(function() {
	var init = function() {
		var category = $("[name=category]");

		category.on("change", function() {
			var title = category.val();

			$("[name=title]").val(title);
			$("[name=url]").val("katalog/" + title.replace(" ", "-").toLowerCase());
		});
	};

	fn.event.push(init);
	init();
});