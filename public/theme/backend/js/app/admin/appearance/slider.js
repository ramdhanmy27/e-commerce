$(document).ready(function() {

	// console.log(Dropzone.options)
	Dropzone.autoDiscover = false;

	var dz = new Dropzone("#slider-photo", {
		paramName: "img",
		parallelUploads: 1,
		maxFilesize: 10,
		url: "product/upload-image",
	    headers: {'X-CSRF-Token': fn.csrf},

	    uploadMultiple: false,
		autoProcessQueue: true,
		addRemoveLinks: true,
		dictResponseError: 'Error while uploading file!',
		dictRemoveFileConfirmation: "Are you sure ?",

		dictDefaultMessage:
			'<span class="bigger-150 bolder"><i class="ace-icon fa fa-caret-right red"></i> Drop files</span> to upload \
			<span class="smaller-80 grey">(or click)</span> <br /> \
			<i class="upload-icon ace-icon fa fa-cloud-upload blue fa-3x"></i>',

		maxfilesexceeded: function(file) {
			dz.removeFile(file)
		}
	});

	dz.on("removedfile", function(file) {
		// remove file on server
		$.ajax({
			url: "product/delete-image",
			method: 'post',
			data: {file: $('[data-dz-name]', file.previewElement).html() || file.name},
			error: function() {
				fn.alertError();
			}
		})
	})

	/*dz.on("addedfile", function(file) {
		dz.removeAllFiles(true);
		fn.alert("You have to set color name first.");
	});*/

	dz.on("success", function(file, data) {
		console.log(data);
		// set filename (for delete image later)
		if (data.length > 0)
			$('[data-dz-name]', file.previewElement).html(data[0]);
	});

	/*dz.on("processing", function() {
		this.options.url = "product/upload-image";
	});*/

	// view stored images
	/*for (var i in $scope.f.data[index].photo) {
		var file = {
			name: $scope.f.data[index].photo[i].name, 
			size: $scope.f.data[index].photo[i].size,
		};

		dz.emit('addedfile', file);
		dz.emit('thumbnail', file, $scope.f.data[index].photo[i].url);
	}*/
})
