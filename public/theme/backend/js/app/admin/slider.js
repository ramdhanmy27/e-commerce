$(document).ready(function() {
	var photoList = [];

	Dropzone.autoDiscover = false;

	var dz = new Dropzone("#slider-photo", {
		url: "slider/upload",
	    headers: {'X-CSRF-Token': fn.csrf},
		paramName: "img",
		parallelUploads: 1,
		maxFilesize: 10,

		clickable: ["#slider-photo", ".dz-clickable"],
	    uploadMultiple: false,
		autoProcessQueue: true,
		addRemoveLinks: true,
		dictResponseError: 'Error while uploading file!',
		dictRemoveFileConfirmation: "Are you sure ?",
	});

	dz.on("addedfile", function(file) {
		photoList.push(file.name);
	});

	dz.on("removedfile", function(file) {
		// remove file on server
		$.ajax({
			url: "slider/delete",
			method: 'post',
			data: {
				filename: $('[data-dz-name]', file.previewElement).html() || file.name
			},
			success: function(data) {
				fn.handleResponse(data).showNotif();
			},
			error: function() {
				fn.alertError();
			},
		})
	})

	dz.on("success", function(file, data) {
		if (photoList.indexOf(data.name) !== -1) {
			dz.removeFile(file);
			fn.notif("Overwrite Successfully", "success");

			return false;
		}

		var response = fn.handleResponse(data).showNotif()
						.error(function() {
							$(file.previewElement).removeClass("dz-success").addClass("dz-error");
						});

		if (!response.isValidResponse()) {
			// set filename (for delete image later)
			if (fn.isset(data, "name")) {
				$('[data-dz-name]', file.previewElement).html(data.name);
				fn.notif("Upload Successfully", "success");
			}
		}
	});

	// view stored images
	$.ajax({
		url: "slider/photos",
		success: function(data) {
			for (var i in data) {
				var file = { name: data[i].name, size: data[i].size };

				dz.emit('addedfile', file);
				dz.emit('thumbnail', file, data[i].url);
			}
		},
	});
})
