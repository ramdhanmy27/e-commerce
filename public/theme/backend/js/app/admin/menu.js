$(document).ready(function() {
	$('#menu-tree').nestable({group: 1})
		.on('change', function(e) {
			var list = e.length ? e : $(e.target);
		});
	
	var nestable = $('#menu-tree').data("nestable");
	var initData = {};

	nestable.dragStart = function(e) {
		var mouse    = this.mouse,
            target   = $(e.target),
            dragItem = target.closest(this.options.itemNodeName);

        initData = {
            id: dragItem.data("id"),
            parent: dragItem.parents("."+this.options.itemClass).data("id"),
            order: dragItem.index(),
        };

        this.placeEl.css('height', dragItem.height());

        mouse.offsetX = e.offsetX !== undefined ? e.offsetX : e.pageX - target.offset().left;
        mouse.offsetY = e.offsetY !== undefined ? e.offsetY : e.pageY - target.offset().top;
        mouse.startX = mouse.lastX = e.pageX;
        mouse.startY = mouse.lastY = e.pageY;

        this.dragRootEl = this.el;

        this.dragEl = $(document.createElement(this.options.listNodeName))
				        .addClass(this.options.listClass + ' ' + this.options.dragClass);
        this.dragEl.css('width', dragItem.width());

        dragItem.after(this.placeEl);
        dragItem[0].parentNode.removeChild(dragItem[0]);
        dragItem.appendTo(this.dragEl);

        $(document.body).append(this.dragEl);

        this.dragEl.css({
            'left' : e.pageX - mouse.offsetX,
            'top'  : e.pageY - mouse.offsetY
        });

        // total depth of dragging item
        var i, depth,
            items = this.dragEl.find(this.options.itemNodeName);

        for (i = 0; i < items.length; i++) {
            depth = $(items[i]).parents(this.options.listNodeName).length;

            if (depth > this.dragDepth)
                this.dragDepth = depth;
        }
	};

	nestable.dragStop = function() {
        var el = this.dragEl.children(this.options.itemNodeName).first();

        el[0].parentNode.removeChild(el[0]);
        this.placeEl.replaceWith(el);
        this.dragEl.remove();
        this.el.trigger('change');

        var data = {
			id: el.data("id"),
			parent: el.parents("."+this.options.itemClass).data("id"),
    		order: el.index(),
		};

        // nothing change
        if (initData.order==data.order && initData.parent==data.parent)
            return;

        $.ajax({
        	url: "menu/order",
        	type: "post",
        	data: {
				id: el.data("id"),
				parent: el.parents("."+this.options.itemClass).data("id"),
        		order: el.index(),
			}
        })

        this.reset();
	}
})
